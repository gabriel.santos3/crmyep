<?php
// created: 2019-06-24 15:02:45
$mod_strings = array (
  'LBL_ALLOW_ALL' => 'Todos',
  'LBL_ALLOW_NONE' => 'Nenhum',
  'LBL_ALLOW_OWNER' => 'Proprietário',
  'LBL_ROLE' => 'Função',
  'LBL_NAME' => 'Nome',
  'LBL_DESCRIPTION' => 'Descrição',
  'LIST_ROLES' => 'Exibir Funções',
  'LBL_USERS_SUBPANEL_TITLE' => 'Usuários',
  'LIST_ROLES_BY_USER' => 'Exibir Funções por Usuário',
  'LBL_ROLES_SUBPANEL_TITLE' => 'Funções do Usuário',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar',
  'LBL_NO_ACCESS' => 'Você não tem acesso a esta área. Por favor, contate o seu administrador de sistemas para obter acesso.',
  'LBL_REDIRECT_TO_HOME' => 'Redirecionar para Início em',
  'LBL_SECONDS' => 'segundos',
  'LBL_ADDING' => 'Adicionando para ',
);