<?php
// created: 2019-06-24 13:31:38
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo Usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_LIST_FORM_TITLE' => 'Lista de gráficos',
  'LBL_MODULE_NAME' => 'Gráficos',
  'LBL_MODULE_TITLE' => 'Gráficos',
  'LBL_HOMEPAGE_TITLE' => 'Meus gráficos',
  'LNK_NEW_RECORD' => 'Criar gráficos',
  'LNK_LIST' => 'Modo de exibição de gráficos',
  'LNK_IMPORT_AOR_CHARTS' => 'Importar gráficos',
  'LBL_SEARCH_FORM_TITLE' => 'Gráficos da pesquisa',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_AOR_CHARTS_SUBPANEL_TITLE' => 'Gráficos',
  'LBL_NEW_FORM_TITLE' => 'Novos gráficos',
);