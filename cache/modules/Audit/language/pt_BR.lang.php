<?php
// created: 2019-06-24 16:42:20
$mod_strings = array (
  'LBL_FIELD_NAME' => 'Campo',
  'LBL_OLD_NAME' => 'Valor Anterior',
  'LBL_NEW_VALUE' => 'Novo Valor',
  'LBL_CREATED_BY' => 'Alterado Por',
  'LBL_LIST_DATE' => 'Data da Alteração',
  'LBL_AUDITED_FIELDS' => 'Campos auditados neste módulo: ',
  'LBL_NO_AUDITED_FIELDS_TEXT' => 'Não existem campos auditados neste módulo',
  'LBL_CHANGE_LOG' => 'Registro de Alterações',
);