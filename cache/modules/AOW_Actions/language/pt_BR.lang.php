<?php
// created: 2019-06-24 13:31:38
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado por Usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_MODULE_NAME' => 'Ações de Fluxo de Serviço',
  'LBL_MODULE_TITLE' => 'Ações de Fluxo de Serviço',
  'LBL_AOW_WORKFLOW_ID' => 'Id AOW_WorkFlow',
  'LBL_ACTION' => 'Ação',
  'LBL_PARAMETERS' => 'Parâmetros',
  'LBL_SENDEMAIL' => 'Escrever E-mail',
  'LBL_CREATERECORD' => 'Criar Registro',
  'LBL_MODIFYRECORD' => 'Modificar Registro',
  'LBL_SELECT_ACTION' => 'Selecione a Ação',
  'LBL_CREATE_EMAIL_TEMPLATE' => 'Criar',
  'LBL_RECORD_TYPE' => 'Tipo de Registro',
  'LBL_ADD_FIELD' => 'Adicionar Campo',
  'LBL_ADD_RELATIONSHIP' => 'Adicionar Relacionamento',
  'LBL_EDIT_EMAIL_TEMPLATE' => 'Editar',
  'LBL_EMAIL' => 'E-mails',
  'LBL_EMAIL_TEMPLATE' => 'Modelo de E-mail',
  'LBL_SETAPPROVAL' => 'Definir aprovação',
  'LBL_RELATE_WORKFLOW' => 'Referente ao Módulo WorkFlow',
  'LBL_INDIVIDUAL_EMAILS' => 'Enviar e-mails Individuais',
);