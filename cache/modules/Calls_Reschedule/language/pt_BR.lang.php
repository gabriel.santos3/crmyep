<?php
// created: 2019-06-24 13:31:38
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_LIST_FORM_TITLE' => 'Lista de Reagendamentos',
  'LBL_MODULE_NAME' => 'Remarcar',
  'LBL_MODULE_TITLE' => 'Remarcar',
  'LBL_HOMEPAGE_TITLE' => 'Meus Reagendamentos',
  'LNK_NEW_RECORD' => 'Criar Reagendamento',
  'LNK_LIST' => 'Remarcar',
  'LNK_IMPORT_CALLS_RESCHEDUAL' => 'Importar Reagendamento',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Reagendamento',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_CALLS_RESCHEDUAL_SUBPANEL_TITLE' => 'Remarcar',
  'LBL_NEW_FORM_TITLE' => 'Novo Reagendamento',
  'LBL_REASON' => 'Razão',
  'LBL_CALLS' => 'ligações',
  'LNK_IMPORT_CALLS_RESCHEDULE' => 'Importar Reagendamento',
  'LBL_CALLS_RESCHEDULE_SUBPANEL_TITLE' => 'Remarcar',
);