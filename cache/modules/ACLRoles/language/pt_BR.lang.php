<?php
// created: 2019-06-24 15:02:45
$mod_strings = array (
  'LBL_MODULE_NAME' => 'Funções',
  'LBL_MODULE_TITLE' => 'Funções: Início',
  'LBL_ROLE' => 'Função',
  'LBL_NAME' => 'Nome',
  'LBL_DESCRIPTION' => 'Descrição',
  'LIST_ROLES' => 'Exibir Funções',
  'LBL_USERS_SUBPANEL_TITLE' => 'Usuários',
  'LIST_ROLES_BY_USER' => 'Exibir Funções por Usuário',
  'LBL_LIST_FORM_TITLE' => 'Funções',
  'LBL_ROLES_SUBPANEL_TITLE' => 'Funções do Usuário',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar',
  'LBL_CREATE_ROLE' => 'Criar Função',
  'LBL_EDIT_VIEW_DIRECTIONS' => 'Duplo clique para alterar o valor da célula.',
  'LBL_ACCESS_DEFAULT' => 'Indefinido',
  'LBL_ACTION_ADMIN' => 'Tipo de Acesso',
  'LBL_ALL' => 'Todos',
  'LBL_DUPLICATE_OF' => 'Duplicar De',
);