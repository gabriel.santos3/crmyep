<?php
// created: 2019-06-24 13:31:35
$mod_strings = array (
  'LBL_MODULE_TITLE' => 'Minhas Pesquisas Salvas',
  'LBL_SEARCH_FORM_TITLE' => 'Minhas Pesquisas Salvas: Pesquisar',
  'LBL_LIST_FORM_TITLE' => 'Minha lista de pesquisas salvas',
  'LBL_DELETE_CONFIRM' => 'Tem certeza que deseja eliminar as pesquisas gravadas selecionadas?',
  'LBL_UPDATE_BUTTON_TITLE' => 'Atualizar esta Pesquisa Gravada',
  'LBL_DELETE_BUTTON_TITLE' => 'Excluir esta pesquisa salva',
  'LBL_SAVE_BUTTON_TITLE' => 'Gravar a Pesquisa atual',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_LIST_MODULE' => 'Módulo',
  'LBL_ORDER_BY_COLUMNS' => 'Ordenar por coluna:',
  'LBL_DIRECTION' => 'Direção:',
  'LBL_SAVE_SEARCH_AS' => 'Gravar esta Pesquisa como:',
  'LBL_SAVE_SEARCH_AS_HELP' => 'Esta operação grava as suas configurações de visualização e qualquer filtro do tabulador do Pesquisa Avançada.',
  'LBL_PREVIOUS_SAVED_SEARCH' => 'Pesquisas anteriormente gravadas:',
  'LBL_PREVIOUS_SAVED_SEARCH_HELP' => 'Editar e Eliminar uma Pesquisa Gravada.',
  'LBL_ASCENDING' => 'Ascendente',
  'LBL_DESCENDING' => 'Descendente',
  'LBL_MODIFY_CURRENT_SEARCH' => 'Modificar pesquisa atual',
);