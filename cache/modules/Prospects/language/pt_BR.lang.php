<?php
// created: 2019-06-24 13:31:34
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DESCRIPTION' => 'Descrição:',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome:',
  'LBL_CREATED_USER' => 'Usuário Criado',
  'LBL_MODIFIED_USER' => 'Usuário Modificado',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_ASSIGNED_TO_ID' => 'Atribuído a',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_SECURITYGROUPS' => 'Grupos de Segurança',
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Grupos de Segurança',
  'LBL_SALUTATION' => 'Saudação',
  'LBL_FIRST_NAME' => 'Primeiro Nome:',
  'LBL_LAST_NAME' => 'Último Nome:',
  'LBL_TITLE' => 'Cargo:',
  'LBL_DEPARTMENT' => 'Departamento:',
  'LBL_DO_NOT_CALL' => 'Não Telefonar:',
  'LBL_HOME_PHONE' => 'Telefone Residencial:',
  'LBL_MOBILE_PHONE' => 'Celular:',
  'LBL_OFFICE_PHONE' => 'Telefone da Empresa',
  'LBL_OTHER_PHONE' => 'Outro Telefone:',
  'LBL_FAX_PHONE' => 'Fax:',
  'LBL_EMAIL_ADDRESS' => 'E-mail:',
  'LBL_PRIMARY_ADDRESS' => 'Endereço Principal:',
  'LBL_PRIMARY_ADDRESS_STREET' => 'Rua do Endereço Principal:',
  'LBL_PRIMARY_ADDRESS_STREET_2' => 'Rua Endereço Principal 2:',
  'LBL_PRIMARY_ADDRESS_STREET_3' => 'Rua Endereço Principal 3:',
  'LBL_PRIMARY_ADDRESS_CITY' => 'Cidade do Endereço Principal:',
  'LBL_PRIMARY_ADDRESS_STATE' => 'Estado do Endereço Principal:',
  'LBL_PRIMARY_ADDRESS_POSTALCODE' => 'Código Postal do Endereço Principal:',
  'LBL_PRIMARY_ADDRESS_COUNTRY' => 'País do Endereço Principal:',
  'LBL_ALT_ADDRESS' => 'Endereço Alternativo',
  'LBL_ALT_ADDRESS_STREET' => 'Rua do Endereço Alternativo:',
  'LBL_ALT_ADDRESS_STREET_2' => 'Outro Endereço Rua 2:',
  'LBL_ALT_ADDRESS_STREET_3' => 'Outro Endereço Rua 3:',
  'LBL_ALT_ADDRESS_CITY' => 'Cidade do Endereço Alternativo:',
  'LBL_ALT_ADDRESS_STATE' => 'Estado do Endereço Alternativo:',
  'LBL_ALT_ADDRESS_POSTALCODE' => 'Código Postal do Endereço Alternativo:',
  'LBL_ALT_ADDRESS_COUNTRY' => 'País do Endereço Alternativo:',
  'LBL_STREET' => 'Outro Endereço',
  'LBL_PRIMARY_STREET' => 'Endereço:',
  'LBL_ALT_STREET' => 'Outro Endereço:',
  'LBL_CITY' => 'Cidade:',
  'LBL_STATE' => 'Estado:',
  'LBL_POSTALCODE' => 'Código Postal',
  'LBL_POSTAL_CODE' => 'Código Postal:',
  'LBL_COUNTRY' => 'País:',
  'LBL_CONTACT_INFORMATION' => 'Informação de Contacto',
  'LBL_ADDRESS_INFORMATION' => 'Informação de Endereço',
  'LBL_OTHER_EMAIL_ADDRESS' => 'Outro E-mail:',
  'LBL_ASSISTANT' => 'Assistente:',
  'LBL_ASSISTANT_PHONE' => 'Telefone do Assistente:',
  'LBL_WORK_PHONE' => 'Telefone de Trabalho',
  'LNK_IMPORT_VCARD' => 'Criar a partir de um vCard',
  'LBL_ANY_EMAIL' => 'Outro E-mail qualquer:',
  'LBL_EMAIL_NON_PRIMARY' => 'Não há emails primários ',
  'LBL_PHOTO' => 'Foto',
  'LBL_MODULE_NAME' => 'Alvos',
  'LBL_MODULE_ID' => 'Alvos',
  'LBL_INVITEE' => 'Relatórios Directos',
  'LBL_MODULE_TITLE' => 'Targets: Tela Principal',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Targets',
  'LBL_LIST_FORM_TITLE' => 'Lista de Targets',
  'LBL_NEW_FORM_TITLE' => 'Novo Target',
  'LBL_PROSPECT' => 'Alvo:',
  'LBL_BUSINESSCARD' => 'Cartão de Visita',
  'LBL_LIST_LAST_NAME' => 'Último Nome',
  'LBL_LIST_PROSPECT_NAME' => 'Nome do Target',
  'LBL_LIST_TITLE' => 'Cargo',
  'LBL_LIST_EMAIL_ADDRESS' => 'E-mail',
  'LBL_LIST_OTHER_EMAIL_ADDRESS' => 'Outro E-mail',
  'LBL_LIST_PHONE' => 'Telefone',
  'LBL_LIST_PROSPECT_ROLE' => 'Função',
  'LBL_LIST_FIRST_NAME' => 'Primeiro Nome',
  'db_last_name' => 'LBL_LIST_LAST_NAME',
  'db_first_name' => 'LBL_LIST_FIRST_NAME',
  'db_title' => 'LBL_LIST_TITLE',
  'db_email1' => 'LBL_LIST_EMAIL_ADDRESS',
  'db_email2' => 'LBL_LIST_OTHER_EMAIL_ADDRESS',
  'LBL_CAMPAIGN_ID' => 'ID Campanha',
  'LBL_EXISTING_PROSPECT' => 'Usado um contato existente',
  'LBL_CREATED_PROSPECT' => 'Criado um novo contato',
  'LBL_EXISTING_ACCOUNT' => 'Usada uma Conta existente',
  'LBL_CREATED_ACCOUNT' => 'Criada uma nova Conta',
  'LBL_CREATED_CALL' => 'Criada uma nova ligação',
  'LBL_CREATED_MEETING' => 'Criada uma nova reunião',
  'LBL_ADDMORE_BUSINESSCARD' => 'Incluir outro cartão de visita',
  'LBL_ADD_BUSINESSCARD' => 'Criar a partir de um Cartão de Visita',
  'LBL_FULL_NAME' => 'Nome',
  'LBL_PROSPECT_NAME' => 'Nome do Target:',
  'LBL_PROSPECT_INFORMATION' => 'Informação do Target',
  'LBL_MORE_INFORMATION' => 'Mais Informações',
  'LBL_ANY_PHONE' => 'Outro Telefone:',
  'LBL_PHONE' => 'Telefone:',
  'LBL_BIRTHDATE' => 'Data de Nascimento:',
  'LBL_EMAIL_OPT_OUT' => 'Não Deseja receber E-mails:',
  'LBL_ALTERNATE_ADDRESS' => 'Endereço Alternativo:',
  'LBL_ANY_ADDRESS' => 'Outro Endereço:',
  'LBL_DESCRIPTION_INFORMATION' => 'Informação de Descrição',
  'LBL_PROSPECT_ROLE' => 'Função:',
  'LBL_OPP_NAME' => 'Nome da Oportunidade:',
  'LBL_IMPORT_VCARD' => 'Importar vCard',
  'LBL_IMPORT_VCARDTEXT' => 'Criar novo contato automaticamente ao importar um vCard dos seus arquivos',
  'LBL_DUPLICATE' => 'Targets Possivelmente Duplicados',
  'MSG_SHOW_DUPLICATES' => 'Ao criar este contato pode estar a duplicar um registro. Poderá clicar em Criar Contato para continuar ou escolher Cancelar.',
  'MSG_DUPLICATE' => 'O registro de alvo que você está prestes a criar pode ser uma duplicata de um registro de alvo já existente. Registros de alvos contendo nomes ou endereços de e-mail semelhantes estão listados abaixo.<br>Clique em salvar para continuar a criar este novo alvo, ou clique em Cancelar para retornar ao módulo sem criar o alvo.',
  'LNK_NEW_ACCOUNT' => 'Criar Nova Conta',
  'LNK_NEW_OPPORTUNITY' => 'Criar Nova Oportunidade',
  'LNK_NEW_CASE' => 'Criar Nova Ocorrência',
  'LNK_NEW_NOTE' => 'Criar Nova Nota',
  'LNK_NEW_CALL' => 'Agendar Nova ligação',
  'LNK_NEW_EMAIL' => 'Arquivar E-mail',
  'LNK_NEW_MEETING' => 'Agendar Nova Reunião',
  'LNK_NEW_TASK' => 'Criar Nova Tarefa',
  'LNK_NEW_APPOINTMENT' => 'Criar Novo Compromisso',
  'LNK_IMPORT_PROSPECTS' => 'Importar Prospectos',
  'NTC_DELETE_CONFIRMATION' => 'Tem a certeza de que pretende eliminar este registro?',
  'NTC_REMOVE_CONFIRMATION' => 'Tem a certeza de que pretende remover este contato desta ocorrência?',
  'NTC_REMOVE_DIRECT_REPORT_CONFIRMATION' => 'Tem a certeza de que pretende remover este registro como um relatório direto?',
  'ERR_DELETE_RECORD' => 'Um número de registro deve ser especificado para excluir o contato.',
  'NTC_COPY_PRIMARY_ADDRESS' => 'Copiar o endereço principal para endereço alternativo',
  'NTC_COPY_ALTERNATE_ADDRESS' => 'Copiar endereço alternativo para endereço principal',
  'LBL_SAVE_PROSPECT' => 'Salvar o Alvo',
  'LBL_CREATED_OPPORTUNITY' => 'Criada uma nova oportunidade',
  'NTC_OPPORTUNITY_REQUIRES_ACCOUNT' => 'Criar uma Oportunidade requer uma Conta.\\n Por favor, cria uma nova ou seleccione uma existente.',
  'LNK_SELECT_ACCOUNT' => 'Seleccionar uma Conta',
  'LNK_NEW_PROSPECT' => 'Novo Target',
  'LNK_PROSPECT_LIST' => 'Targets',
  'LNK_NEW_CAMPAIGN' => 'Criar Nova Campanha',
  'LNK_CAMPAIGN_LIST' => 'Campanhas',
  'LNK_NEW_PROSPECT_LIST' => 'Nova Lista de Targets',
  'LNK_PROSPECT_LIST_LIST' => 'Listas de Targets',
  'LNK_IMPORT_PROSPECT' => 'Importar Targets',
  'LBL_SELECT_CHECKED_BUTTON_LABEL' => 'Seleccionar Targets Marcados',
  'LBL_SELECT_CHECKED_BUTTON_TITLE' => 'Seleccionar Targets Marcados',
  'LBL_INVALID_EMAIL' => 'E-mail Inválido:',
  'LBL_DEFAULT_SUBPANEL_TITLE' => 'Alvos',
  'LBL_PROSPECT_LIST' => 'Lista de Potenciais Clientes',
  'LBL_CONVERT_BUTTON_KEY' => 'V',
  'LBL_CONVERT_BUTTON_TITLE' => 'Converter Target',
  'LBL_CONVERT_BUTTON_LABEL' => 'Converter Target',
  'LBL_CONVERTPROSPECT' => 'Converter Target',
  'LNK_NEW_CONTACT' => 'Novo contato',
  'LBL_CREATED_CONTACT' => 'Criado um novo contato',
  'LBL_BACKTO_PROSPECTS' => 'Voltar aos Targets',
  'LBL_CAMPAIGNS' => 'Campanhas',
  'LBL_CAMPAIGN_LIST_SUBPANEL_TITLE' => 'registro de Campanha',
  'LBL_TRACKER_KEY' => 'Chave do Rastreador',
  'LBL_LEAD_ID' => 'Id da Lead',
  'LBL_CONVERTED_LEAD' => 'Lead Convertida',
  'LBL_ACCOUNT_NAME' => 'Nome da Conta',
  'LBL_EDIT_ACCOUNT_NAME' => 'Nome da Conta:',
  'LBL_CAMPAIGNS_SUBPANEL_TITLE' => 'Campanhas',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Histórico',
  'LBL_PHONE_HOME' => 'Telefone Residencial',
  'LBL_PHONE_MOBILE' => 'Celular',
  'LBL_PHONE_WORK' => 'Telefone Comercial',
  'LBL_PHONE_OTHER' => 'Outro Telefone',
  'LBL_PHONE_FAX' => 'Fax:',
  'LBL_EXPORT_ASSIGNED_USER_NAME' => 'nome de usuário atribuído',
  'LBL_EXPORT_ASSIGNED_USER_ID' => 'ID do usuário atribuído',
  'LBL_EXPORT_MODIFIED_USER_ID' => 'Modificado Por ID',
  'LBL_EXPORT_CREATED_BY' => 'Criado pelo ID',
  'LBL_EXPORT_EMAIL2' => 'Outro E-mail',
  'LBL_FP_EVENTS_PROSPECTS_1_FROM_FP_EVENTS_TITLE' => 'Eventos',
);