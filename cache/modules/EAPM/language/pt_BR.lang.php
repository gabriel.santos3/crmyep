<?php
// created: 2019-06-24 13:31:35
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado Por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Usuário do App',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Usuário SuiteCRM',
  'LBL_TEAM' => 'Equipes',
  'LBL_TEAMS' => 'Equipes',
  'LBL_TEAM_ID' => 'ID da Equipe',
  'LBL_LIST_FORM_TITLE' => 'Lista de Conta Externa',
  'LBL_MODULE_NAME' => 'Conta Externa',
  'LBL_MODULE_TITLE' => 'Contas Externas',
  'LBL_HOMEPAGE_TITLE' => 'Minhas Contas Externas',
  'LNK_NEW_RECORD' => 'Criar Conta Externa',
  'LNK_LIST' => 'Ver Contas Externas',
  'LNK_IMPORT_SUGAR_EAPM' => 'Importar Contas Externas',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar em Fonte Externa',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_SUGAR_EAPM_SUBPANEL_TITLE' => 'Contas Externas',
  'LBL_NEW_FORM_TITLE' => 'Nova Conta Externa',
  'LBL_PASSWORD' => 'Senha da App',
  'LBL_USER_NAME' => 'Nome do Usuário da App',
  'LBL_URL' => 'URL:',
  'LBL_APPLICATION' => 'Aplicação',
  'LBL_API_DATA' => 'Dados da API',
  'LBL_API_TYPE' => 'Tipo de Autenticação',
  'LBL_API_CONSKEY' => 'Chave do Consumidor',
  'LBL_API_CONSSECRET' => 'Segredo do consumidor',
  'LBL_API_OAUTHTOKEN' => 'Token OAuth',
  'LBL_AUTH_UNSUPPORTED' => 'Este método de autorização não é suportado pela aplicação',
  'LBL_AUTH_ERROR' => 'Tentativa de autenticação com a conta externa falhou.',
  'LBL_VALIDATED' => 'Acesso Validado',
  'LBL_ACTIVE' => 'Activo',
  'LBL_OAUTH_NAME' => '%s',
  'LBL_SUGAR_USER_NAME' => 'Usuário SuiteCRM',
  'LBL_DISPLAY_PROPERTIES' => 'Propriedades do Display',
  'LBL_CONNECT_BUTTON_TITLE' => 'Conectar',
  'LBL_NOTE' => 'Notas',
  'LBL_CONNECTED' => 'Conectado',
  'LBL_DISCONNECTED' => 'Não conectado',
  'LBL_ERR_NO_AUTHINFO' => 'Não existe informação de autenticação para esta conta.',
  'LBL_ERR_NO_TOKEN' => 'Não existe tokens válidos de autenticação para esta conta.',
  'LBL_ERR_FAILED_QUICKCHECK' => 'Não está autenticado atualmente na sua conta {0}. Carregar em OK para fazer nova autenticação na sua conta para ativar o registo da conta externa.',
  'LBL_CLICK_TO_EDIT' => 'Clique para Editar',
  'LBL_MEET_NOW_BUTTON' => 'Reunir Agora',
  'LBL_VIEW_LOTUS_LIVE_MEETINGS' => 'Ver as Próximas Reuniões LotusLive&#153;',
  'LBL_TITLE_LOTUS_LIVE_MEETINGS' => 'Próximas Reuniões LotusLive&#153;',
  'LBL_VIEW_LOTUS_LIVE_DOCUMENTS' => 'Ver Documentos LotusLive&#153;',
  'LBL_TITLE_LOTUS_LIVE_DOCUMENTS' => 'Documentos LotusLive&#153;',
  'LBL_REAUTHENTICATE_LABEL' => 'Repetir autenticação',
  'LBL_REAUTHENTICATE_KEY' => 'a',
  'LBL_APPLICATION_FOUND_NOTICE' => 'Uma conta para esta aplicação já existe. Foi reintegrada a conta existente.',
  'LBL_OMIT_URL' => '(Omitir http:// ou https://)',
  'LBL_OAUTH_SAVE_NOTICE' => 'Clique em <b>Conectar</b> para ser direcionado a uma página para fornecer as informações de sua conta e para autorizar o acesso à conta pelo SuiteCRM. Após a conexão, você será direcionado de volta ao SuiteCRM.',
  'LBL_BASIC_SAVE_NOTICE' => 'Clique em <b>Conectar</b> para conectar esta conta ao SuiteCRM.',
  'LBL_ERR_FACEBOOK' => 'O Facebook retornou um erro e o feed não pode ser mostrado.',
  'LBL_ERR_OAUTH_FACEBOOK_1' => 'Autenticação do Facebook não foi efetuada. Por favor, tente novamente',
  'LBL_ERR_OAUTH_FACEBOOK_2' => 'Login do Facebook novamente ',
  'LBL_ERR_NO_RESPONSE' => 'Um erro ocorreu quando foi tentado gravar para a conta externa.',
  'LBL_ERR_TWITTER' => 'O Twitter retornou um erro, e o feed não pode ser mostrado.',
  'LBL_ERR_POPUPS_DISABLED' => 'Favor habilitar o Popup ou adicionar excessões para o website "{0}" na lista de excessões para conectar.',
);