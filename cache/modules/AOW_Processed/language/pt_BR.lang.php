<?php
// created: 2019-06-24 13:31:38
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_LIST_FORM_TITLE' => 'Lista de Auditoria de Processos',
  'LBL_MODULE_NAME' => 'Auditoria em Processo',
  'LBL_MODULE_TITLE' => 'Auditoria em Processo',
  'LBL_HOMEPAGE_TITLE' => 'Minhas Auditorias de Processos',
  'LNK_NEW_RECORD' => 'Criar Auditoria de Processo',
  'LNK_LIST' => 'Exibir Auditoria do Processo',
  'LNK_IMPORT_FF_AOW_PROCESSED' => 'Importar Auditoria de Processo',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Auditoria de Processo',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_FF_AOW_PROCESSED_SUBPANEL_TITLE' => 'Auditoria em Processo',
  'LBL_NEW_FORM_TITLE' => 'Nova Auditoria do Processo',
  'LBL_AOW__WORKFLOW_ID' => 'Identificação do fluxo de trabalho',
  'LBL_AOW__WORKFLOW' => 'Fluxo de Serviço',
  'LBL_AOW_ACTION_ID' => 'Id da Ação',
  'LBL_AOW_ACTION' => 'Ação',
  'LBL_MODULE' => 'Módulo',
  'LBL_BEAN' => 'Registro',
  'LBL_STATUS' => 'Estado',
  'LNK_IMPORT_AOW_AOW_PROCESSED' => 'Importar Auditoria de Processo',
  'LBL_AOW_AOW_PROCESSED_SUBPANEL_TITLE' => 'Auditoria em Processo',
  'LNK_IMPORT_AOW_PROCESSED' => 'Importar Auditoria de Processo',
  'LBL_AOW_PROCESSED_SUBPANEL_TITLE' => 'Auditoria em Processo',
  'LBL_AOW_WORKFLOW' => 'Work Flow',
  'LNK_NEW_WORKFLOW' => 'Criar WorkFlow',
  'LNK_WORKFLOW_LIST' => 'Fluxo de Serviço',
);