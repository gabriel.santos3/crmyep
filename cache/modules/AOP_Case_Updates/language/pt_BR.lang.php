<?php
// created: 2019-06-24 13:31:38
$mod_strings = array (
  'LBL_ID' => 'ID',
  'LBL_DATE_ENTERED' => 'Data de Criação',
  'LBL_DATE_MODIFIED' => 'Data de Modificação',
  'LBL_MODIFIED' => 'Modificado Por',
  'LBL_MODIFIED_ID' => 'Modificado Por Id',
  'LBL_MODIFIED_NAME' => 'Modificado Por Nome',
  'LBL_CREATED' => 'Criado por',
  'LBL_CREATED_ID' => 'Criado Por Id',
  'LBL_DESCRIPTION' => 'Descrição',
  'LBL_DELETED' => 'Excluído',
  'LBL_NAME' => 'Nome',
  'LBL_CREATED_USER' => 'Criado pelo usuário',
  'LBL_MODIFIED_USER' => 'Modificado pelo usuário',
  'LBL_LIST_NAME' => 'Nome',
  'LBL_EDIT_BUTTON' => 'Editar',
  'LBL_REMOVE' => 'Remover',
  'LBL_ASSIGNED_TO_ID' => 'ID do usuário atribuído',
  'LBL_ASSIGNED_TO_NAME' => 'Atribuído a',
  'LBL_LIST_FORM_TITLE' => 'Lista Atual de Ocorrência',
  'LBL_MODULE_NAME' => 'Tipos de atualizações',
  'LBL_MODULE_TITLE' => 'Tipos de atualizações',
  'LBL_HOMEPAGE_TITLE' => 'Minhas Ocorrências Atuais',
  'LNK_NEW_RECORD' => 'Criar Ocorrência',
  'LNK_LIST' => 'Visualizar Ocorrência Atual',
  'LNK_IMPORT_AOP_AOP_CASE_UPDATES' => 'Importar Ocorrência Atual',
  'LBL_SEARCH_FORM_TITLE' => 'Busca de Ocorrências',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'Ver Histórico',
  'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Atividades',
  'LBL_AOP_AOP_CASE_UPDATES_SUBPANEL_TITLE' => 'Tipos de atualizações',
  'LBL_NEW_FORM_TITLE' => 'Novo Ocorrência',
  'LNK_IMPORT_AOP_CASE_UPDATES' => 'Importar Ocorrência Atual',
  'LBL_AOP_CASE_UPDATES_SUBPANEL_TITLE' => 'Tipos de atualizações',
  'LBL_CASE_NAME' => 'Ocorrências',
  'LBL_CONTACT_NAME' => 'Contatos',
  'LBL_INTERNAL' => 'Atualização interna',
  'LBL_AOP_CASE_ATTACHMENTS' => 'Anexos: ',
);