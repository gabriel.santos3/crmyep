<?php
// created: 2016-10-06 16:23:29
$dictionary["f_marcas_aos_products"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'f_marcas_aos_products' => 
    array (
      'lhs_module' => 'F_Marcas',
      'lhs_table' => 'f_marcas',
      'lhs_key' => 'id',
      'rhs_module' => 'AOS_Products',
      'rhs_table' => 'aos_products',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'f_marcas_aos_products_c',
      'join_key_lhs' => 'f_marcas_aos_productsf_marcas_ida',
      'join_key_rhs' => 'f_marcas_aos_productsaos_products_idb',
    ),
  ),
  'table' => 'f_marcas_aos_products_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'f_marcas_aos_productsf_marcas_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'f_marcas_aos_productsaos_products_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'f_marcas_aos_productsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'f_marcas_aos_products_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'f_marcas_aos_productsf_marcas_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'f_marcas_aos_products_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'f_marcas_aos_productsaos_products_idb',
      ),
    ),
  ),
);