<?php
// created: 2016-10-06 16:23:29
$dictionary["AOS_Products"]["fields"]["f_marcas_aos_products"] = array (
  'name' => 'f_marcas_aos_products',
  'type' => 'link',
  'relationship' => 'f_marcas_aos_products',
  'source' => 'non-db',
  'module' => 'F_Marcas',
  'bean_name' => false,
  'vname' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
  'id_name' => 'f_marcas_aos_productsf_marcas_ida',
);
$dictionary["AOS_Products"]["fields"]["f_marcas_aos_products_name"] = array (
  'name' => 'f_marcas_aos_products_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
  'save' => true,
  'id_name' => 'f_marcas_aos_productsf_marcas_ida',
  'link' => 'f_marcas_aos_products',
  'table' => 'f_marcas',
  'module' => 'F_Marcas',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["f_marcas_aos_productsf_marcas_ida"] = array (
  'name' => 'f_marcas_aos_productsf_marcas_ida',
  'type' => 'link',
  'relationship' => 'f_marcas_aos_products',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
);
