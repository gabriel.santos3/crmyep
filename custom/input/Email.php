<?php 

	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 25/01/2019
	*	VERSÃO: 1.0
	*
	*	ESTA CLASSE É RESPONSÁVEL POR GERENCIAR TUDO SOBRE ENVIO DE E-MAILS.
	*/

	class Email
	{
		private $fileLog;
		private $mail;
		
		/*!
		*	CARREGA AS DIRETRIZES INICIAIS PARA PODER FAZER O ENVIO DE E-MAILS.
		*/
		public function __construct()
		{
			date_default_timezone_set('America/Sao_Paulo');//seta o fuso horário correto
			
			$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/input/logs/exception.log","a");//define o arquivo de log
			
			require("phpmailer/PHPMailerAutoload.php");
			$this->mail = new PHPMailer;
			$this->mail->Charset = 'UTF-8';
			$this->mail->isSMTP();
			$this->mail->isHTML(true);
			$this->mail->SMTPDebug = 0;
			$this->mail->Debugoutput = 'html';
			$this->mail->Host = "smtp.gmail.com";
			$this->mail->Port = 587;
			$this->mail->SMTPAuth = true;
			$this->mail->SMTPSecure = 'tls';
			
			//usuário remetente
			
			$this->mail->Username = "no-reply@primeinterway.com.br";
			$this->mail->Password = "prime!nterw@y";
			$this->mail->setFrom('no-reply@primeinterway.com.br', 'SuiteCRM');
		}
		/*!
		*	RESPONSÁVEL POR CRIAR UM E-MAIL PARA O VENDEDOR ATRIBUÍDO A OPORTUNIDADE.
		*
		*	$Email-> Objeto que contém os dados para montar o e-mail a ser enviado.
		*/
		public function CriaEmail($Email)
		{
			try{
				$this->EnviaEmail($Email->getNome(), $Email->getEmail(), $Email->getAssunto(), $Email->getMensagem());
			}catch(Exception $e)
			{
				$texto = "\r\n----------EXCEPTION E-MAIL--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION E-MAIL--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	RESPONSÁVEL POR ENVIAR UM E-MAIL.
		*
		*	$nome -> Nome do destinatário.
		*	$email -> E-mail do destinatário.
		*	$assunto -> Assunto ao que se refere o e-mail.
		*	$mensagem -> Conteúdo para o corpo do e-mail. Pode conter HTML.
		*/
		public function EnviaEmail($nome, $email, $assunto, $mensagem)
		{
			$this->mail->addAddress($email, $nome);
			$this->mail->Subject = $assunto;
			$this->mail->Body = $mensagem;
			
			if (!$this->mail->send())//caso falhe o envio, então registra no log
			{
				$texto = "\r\n----------EXCEPTION E-MAIL--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$this->mail->ErrorInfo;
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION E-MAIL--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	ESCREVE O LOG NO ARQUIVO.
		*	
		*	$texto -> Contém o texto de log a ser escrito.
		*/
		function WriteFile($texto)
		{
			fwrite($this->fileLog, $texto." \r\n");
		}
		/*!
		*	RSPONSÁVEL POR DESTRUIR O PONTEIRO QUE CONTÉM A INSTÂNCIA DO ARQUIVO DE LOG.
		*/
		public function __destruct()
		{
			fclose($this->fileLog);
		}
	}