<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 29/01/2019
	*	VERSÃO: 1.0
	*	
	*	ESTA CLASSE TEM POR OBJETIVO REALIZAR A INSERÇÃO DE OPORTUNIDADES DE TODA CONVERSÃO OCORRIDA 
	*	ATRAVÉS DO FORMULÁRIO DE CONTAO DO BUYPRIME.
	*/
	
	require_once("settings/defines.php");
	
	require_once("generic/GenericOpportunity.php");
	
	class OportunidadeContatoPrimeEmMktEntryPoint extends GenericOpportunity 
	{
		/*!
		*	RESPONSÁVEL POR CARREGAR AS DIRETRIZES INICIAIS DA INTEGRAÇÃO.
		*/
		public function __construct()
		{
			parent::__construct();
		}
		/*!
		*	RESPONSÁVEL POR CARREGAR O OBJETO DE CONTA E ENVIA-LO PARA A CLASSE DE CADASTRO/UPDATE DE CONTAS.
		*
		*	$dados -> Objeto JSON enviado pelo RD Station.
		*/
		public function CarregaConta($dados)
		{
			require_once("models/Account_model.php");
			require_once("generic/GenericAccount.php");
			$AccountG = new GenericAccount();
			
			$AccountBd = $AccountG->GetAccount($dados->leads[0]->last_conversion->content->email_lead);
			
			$Account = new Account_model();
			$Account->setEmpresa($dados->leads[0]->last_conversion->content->Empresa);
			$Account->setEmail($dados->leads[0]->last_conversion->content->email_lead);
			$Account->setTelefone($dados->leads[0]->last_conversion->content->Telefone);
			$Account->setCnpj($dados->leads[0]->last_conversion->content->CNPJ);
			$Account->setAtribuidoA(ADMIN);
			$Account->setTipo("Reseller");
			
			//VERIFICAR SE DEVE CRIAR A CONTA OU ATUALIZA-LA.
			if($AccountBd == null)
			{
				$AccountG->CreateAccount($Account);
				$Account = $AccountG->GetAccount($Account->getEmail());
			}
			else 
			{
				$Account->setId($AccountBd->getId());
				$AccountG->UpdateAccount($Account);
			}
			return $Account;
		}
		/*!
		*	RESPONSÁVEL POR CARREGAR OS DADOS DE CONTATO E ENVIA-LO A CLASE CADASTRO DE CONTATO DE CLIENTE/REVENDA.
		*
		*	$dados -> Objeto JSON enviado pelo RD Station.
		*	$account_id -> Id da conta para associar ao contato.
		*/
		public function CarregaContato($dados, $account_id)
		{
			require_once("generic/GenericContact.php");
			require_once("models/Contact_model.php");
			
			$ContactG = new GenericContact();
			$ContactBd = $ContactG->GetContact($dados->leads[0]->last_conversion->content->email_lead);

			$Contact = new Contact_model();
			$Contact->setNome($dados->leads[0]->last_conversion->content->Nome);
			$Contact->setEmail($dados->leads[0]->last_conversion->content->email_lead);
			$Contact->setContaId($account_id);
			$Contact->setTelefoneComercial($dados->leads[0]->last_conversion->content->Telefone);
			$Contact->setOrigem("E-mail Marketing");
			$Contact->setAtribuidoA(ADMIN);
			
			if($ContactBd == null)
			{
				$ContactG->CreateContact($Contact);
				$Contact = $ContactG->GetContact($Contact->getEmail());
			}
			else
			{
				$Contact->setId($ContactBd->getId());
				$ContactG->UpdateContact($Contact);
			}
		}
		/*!
		*	RESPONSÁVEL POR RECEBER E TRATAR O FLUXO (CRIAR CONTA, BUSCAR CONTA, CRIAR OPORTUNIDADE).
		*/
		public function Load()
		{
			header("Content-Type: application/json");
			$dados = json_decode(stripslashes(file_get_contents("php://input")));
			
			require_once("models/Opportunity_model.php");
			require_once("models/Email_model.php");
			
			require_once("generic/GenericUser.php");
			
			$Account = $this->CarregaConta($dados);
			$this->CarregaContato($dados, $Account->getId());
			
			$Email = new Email_model();
			
			$Opportunity = new Opportunity_model();
			$nome = explode(" ", $dados->leads[0]->last_conversion->content->Vendedor);
			$Usuario = new GenericUser();

			$ObjUs = $Usuario->GetUsuario($nome[0], $nome[1]);
			///VERIFICAR SE DEVE ATRIBUIR AO EMERSON OU A UM VENDEDOR.
			if($ObjUs == null)
			{
				$Email->setNome("Emerson");
				$Email->setEmail("emerson.fonseca@primeinterway.com.br");//buscar pelo getUser
				$Opportunity->setAtribuidoA(EMERSON);
			}
			else
			{
				$Opportunity->setAtribuidoA($ObjUs->getId());
				$Email->setNome($ObjUs->getPrimeiroNome());
				$Email->setEmail($ObjUs->getEmail());//buscar pelo getUser
			}
			
			$Opportunity->setNome("Contato E-mail Marketing");
			$Opportunity->setDescricao($dados->leads[0]->last_conversion->content->Mensagem);
			$Opportunity->setIdRevenda($Account->getId());
			
			$Opportunity->setOrigem("E-mail Marketing");
			$Opportunity->setFaseVenda("Prospeccao");
			
			if($this->GetOpportunity($Opportunity) == null)
			{
				//cadastra
				$this->CreateOpportunity($Opportunity);
				//recupera a id da oportunidade recém cadastrada.
				$op_id = $this->GetOpportunity($Opportunity)->getId();
				//coloca a id no objeto de oportunidade.

				$Opportunity->setId($op_id);
				//envia e-mail
				
				$this->LancaTarefa($Opportunity);
				
				$Email->setAssunto("CRM - Nova oportunidade");
				$Email->setModulo($Opportunity);
				$mensagem = "Olá2 ".$Email->getNome().". <br /><br /> Uma nova oportunidade foi registrada no SuiteCRM.<br /><br />";
				$mensagem .= "Segue o link abaixo para acessa-la.<br /><br />";
				$mensagem .= $this->url."/index.php?action=ajaxui#ajaxUILoc=index.php%3Fmodule%3DOpportunities%26offset%3D1%26stamp%3D1547140384068769300%26return_module%3DOpportunities%26action%3DDetailView%26record%3D".$Email->getModulo()->getId();
				$Email->setMensagem($mensagem);
				include("Email.php");
				$em = new Email();
				$em->CriaEmail($Email);
			}
		}
		/*
		*	RESPONSÁVEL POR ENVIAR OS DADOS DE ATIVIDADE PARA A CLASSE DE LANÇAMENTO DE ATIVIDADES.
		*
		*	$Opportunity -> Oportunidade criada.
		*/
		private function LancaTarefa($Opportunity)
		{
			include("models/Task_model.php");
			$Task = new Task_model();
			$Task->setAssunto("Enviar e-mail de cotação");
			$Task->setDescricao("Usar template proposta comercial para inserir produtos, descrição e preços.");
			$Task->setPrioridade("High");
			$Task->setModuloReferente("Opportunities");
			$Task->setModuloId($Opportunity->getId());
			$Task->setAtribuidoA($Opportunity->getAtribuidoA());
			include("generic/GenericTask.php");
			$TaskC = new GenericTask();
			$TaskC->CreateTask($Task);
		}
	}

	$r = new OportunidadeContatoPrimeEmMktEntryPoint();
	$r->Load();