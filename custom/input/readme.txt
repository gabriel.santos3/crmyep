Ao criar uma nova classe que em algum m�todo que a comp�e houver a necessidade de criar ou fazer leitura de algum registro 
no CRM, jamais dar o nome igual a algum m�dulo existente no CRM.

-----

Adicionar mais op��es de origem de oportunidade:

crmAlpha\custom\include\language\pt_BR.lang

-----

e-mail das contas ficam na tabela email_address

o relacionamento entre a tabela accounts e e tabela email_address fica na tabela: email_addr_bean_rel

-----

						ATEN��O!!!
Tomar cuidado ao modificar a model de e-mail, os defines e a classe de envio de e-mail (Email.php), pois estes arquivos 
est�o sendo utilizados para notificar o vendedor toda vez que a oportunidade � atribu�da a eles.
A parte da integra��o respons�vel por isso encontra-se no seguinte diret�rio: custom/modules/opportunities.


INSTRU��ES PARA TESTE
� RECOMENDADO EXTRAIR UM OBJETO JSON DE CADA CEN�RIO, CRIAR UMA APLICA��O PHP QUE FA�A O ENVIO DO OBJETO PARA A URL 
DO CRM, EM VEZ DE FICAR SE CONVERTENDO NOS FORMUL�RIOS DO RD. ASSIM TEREMOS UMA SIMULA��O QUE PERMITE DEBUGAR COM MAIS 
FACILIDADE A INTEGRA��O.