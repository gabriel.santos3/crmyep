<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 29/01/2019
	*	VERSÃO: 1.0
	*	
	*	ESTA CLASSE TEM POR OBJETIVO REALIZAR A INSERÇÃO DE OPORTUNIDADES DE TODA CONVERSÃO OCORRIDA 
	*	ATRAVÉS DOS FORMULÁRIOS DE BAIXAR CONTEÚDO.
	*/

	require_once("settings/defines.php");
	
	require_once("generic/GenericOpportunity.php");
	
	class OportunidadeDownloadConteudoEntryPoint extends GenericOpportunity 
	{
		/*!
		*	RESPONSÁVEL POR CARREGAR AS DIRETRIZES INICIAIS DA INTEGRAÇÃO.
		*/
		public function __construct()
		{
			parent::__construct();
		}
		/*!
		*	COMO A DESCRIÇÃO DA OPORTUNIDADE PARA ESTE CENÁRIO VAI SER SEMPRE 'estudo-saude-zebra' INDEPENDENTE 
		*	DA REVENDA, ENTÃO O MÉTODO ABAIXO DEVE FAZER UMA BUSCA MAIS RIGOROSA EM BUSCA DA OPORTUNIDADE JÁ CADASTRADA, 
		*	O MÉTODO DA SUPERCLASSE JÁ IDENTIFICAR ISSO, PORÉM APENAS LEVA EM CONSIDERAÇÃO O A DESCRIÇÃO DA OPORTUNIDADE, 
		*	ISTO É, ELE PERMITE QUE A OPORTUNIDADE SEJA INSERIDA APENAS UMA VEZ NO BANCO DE DADOS.
		*	
		*	OBS.: A CONSULTA ABAIXO BUSCA COM BASE NO E-MAIL DO LEAD, PORÉM SÓ SERVE PARA BUSCAR SE O E-MMAIL FOR REVENDA,
		*	ATRIBUÍDA A OPORTUNIDADE. NESTE CENÁRIO APENAS REVENDAS ENVIAM DADS, POR ISSO O USO DESTE MÉTODO E NÃO O
		*	DA SUPERCLASSE  PARA CLIENTE FINAL A CONSULTA SEGUE UM CAMINHO DIFERENTE.
		*
		*	$Opportunity -> Objeto oportunidade.
		*	$email_lead -> E-mail da REVENDA para validar se já existe uma oportunidade para ela.
		*/
		public function GetOpportunity($Opportunity, $email_lead = FALSE)
		{
			include("settings/conexao.php");
			require_once("models/Opportunity_model.php");
			$Op = new Opportunity_model();
			
			
			$deletado = 0;
			$descricao = $Opportunity->getDescricao();
			$dataCriacao = $Opportunity->getDataCriacao();
			try
			{
				$query = $conexao->prepare("
					SELECT o.id, o.name, o.description, o.lead_source, o.assigned_user_id, o.date_entered, ea.email_address 
					FROM opportunities o 
					inner join opportunities_cstm oc on oc.id_c = o.id 
                    inner join accounts a on a.id = oc.account_id_c 
                    inner join email_addr_bean_rel er on er.bean_id = a.id 
                    inner join email_addresses ea on ea.id = er.email_address_id
                    
                    WHERE o.description = ? 
					AND o.deleted = ? 
                    AND DATE_FORMAT(o.date_entered, '%Y-%m-%d') = ? 
                    AND ea.email_address = ?");
	
				$query->bindParam(1, $descricao);
				$query->bindParam(2, $deletado);
				$query->bindParam(3, $dataCriacao);
				$query->bindParam(4, $email_lead);
				
				$query->execute();
				
				if($query->rowCount() == 0)
					return null;
				else
				{
					$query = $query->fetch(PDO::FETCH_OBJ);
					$Opportunity->setId($query->id);
					return $Opportunity;
				}
			}
			catch(PDOException $e)
			{
				$texto = "\r\n----------EXCEPTION QUERY OPPORTUNITY DOWNLOAD CONTEÚDO--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n---------EXCEPTION QUERY OPPORTUNITY DOWNLOAD CONTEÚDO--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
				return new Opportunity_model();
			}
		}
		/*!
		*	RESPONSÁVEL POR CARREGAR O OBJETO DE CONTA E ENVIA-LO PARA A CLASSE DE CADASTRO/UPDATE DE CONTAS.
		*
		*	$dados -> Objeto JSON enviado pelo RD Station.
		*/
		public function CarregaConta($dados)
		{
			require_once("models/Account_model.php");
			require_once("generic/GenericAccount.php");
			$AccountG = new GenericAccount();
			
			$AccountBd = $AccountG->GetAccount($dados->leads[0]->last_conversion->content->email_lead);
			
			$Account = new Account_model();
			$Account->setEmpresa($dados->leads[0]->last_conversion->content->Empresa);
			$Account->setEmail($dados->leads[0]->last_conversion->content->email_lead);
			$Account->setAtribuidoA(ADMIN);
			$Account->setTipo("Reseller");
			if($AccountBd == null)
			{
				$AccountG->CreateAccount($Account);
				$Account = $AccountG->GetAccount($Account->getEmail());
			}
			else 
			{
				$Account->setId($AccountBd->getId());
				$AccountG->UpdateAccount($Account);
			}
			return $Account;
		}
		/*!
		*	RESPONSÁVEL POR CARREGAR OS DADOS DE CONTATO E ENVIA-LO A CLASE CADASTRO CONTATO DE CLIENTE/REVENDA.
		*
		*	$dados -> Objeto JSON enviado pelo RD Station.
		*	$account_id -> Id da conta para associar ao contato.
		*/
		public function CarregaContato($dados, $account_id)
		{
			require_once("generic/GenericContact.php");
			require_once("models/Contact_model.php");
			
			$ContactG = new GenericContact();
			$ContactBd = $ContactG->GetContact($dados->leads[0]->last_conversion->content->email_lead);

			$Contact = new Contact_model();
			$Contact->setNome($dados->leads[0]->last_conversion->content->Nome);
			$Contact->setEmail($dados->leads[0]->last_conversion->content->email_lead);
			$Contact->setContaId($account_id);
			$Contact->setCargo($dados->leads[0]->last_conversion->content->Cargo);
			$Contact->setTelefoneComercial($dados->leads[0]->last_conversion->content->Telefone);
			$Contact->setOrigem("Landing page");
			$Contact->setAtribuidoA(ADMIN);
			
			if($ContactBd == null)
			{
				$ContactG->CreateContact($Contact);
				$Contact = $ContactG->GetContact($Contact->getEmail());
			}
			else
			{
				$Contact->setId($ContactBd->getId());
				$ContactG->UpdateContact($Contact);
			}
		}
		/*!
		*	RESPONSÁVEL POR RECEBER E TRATAR O FLUXO (CRIAR CONTA, BUSCAR CONTA, CRIAR OPORTUNIDADE).
		*/
		public function Load()
		{
			header("Content-Type: application/json");
			$dados = json_decode(stripslashes(file_get_contents("php://input")));
			
			require_once("models/Opportunity_model.php");
			require_once("models/Email_model.php");
			
			require_once("generic/GenericUser.php");
			
			$Email = new Email_model();
			
			$Account = $this->CarregaConta($dados);
			$this->CarregaContato($dados, $Account->getId());
			
			$Opportunity = new Opportunity_model();
			
			$Opportunity->setNome("Download de conteúdo");
			$Opportunity->setDescricao($dados->leads[0]->last_conversion->content->identificador);
			$Opportunity->setIdRevenda($Account->getId());
			preg_match("/\d{4}-\d{2}-\d{2}/", $dados->leads[0]->last_conversion->content->created_at, $match);
			$Opportunity->setDataCriacao($match[0]);
			
			$Opportunity->setOrigem("Landing page");
			$Opportunity->setFaseVenda("Prospeccao");
			
			$nome = explode(" ", $dados->leads[0]->last_conversion->content->{"Vendedor"});//"Vendedor Zebra"
			$Usuario = new GenericUser();
			$ObjUs = $Usuario->GetUsuario($nome[0], $nome[1]);
			///VERIFICAR SE DEVE ATRIBUIR AO EMERSON OU A UM VENDEDOR.
			if($ObjUs == null)
			{
				$Email->setNome("Emerson");
				$Email->setEmail("emerson.fonseca@primeinterway.com.br");//buscar pelo getUser
				$Opportunity->setAtribuidoA(EMERSON);
			}
			else
			{
				$Opportunity->setAtribuidoA($ObjUs->getId());
				$Email->setNome($ObjUs->getPrimeiroNome());
				$Email->setEmail($ObjUs->getEmail());//buscar pelo getUser
			}
			
			if($this->GetOpportunity($Opportunity, $Account->getEmail()) == null)
			{
				//cadastra
				$this->CreateOpportunity($Opportunity);
				//recupera a id da oportunidade recém cadastrada.
				$op_id = $this->GetOpportunity($Opportunity, $Account->getEmail())->getId();
				//coloca a id no objeto de oportunidade.

				$Opportunity->setId($op_id);
				//envia e-mail
				
				$this->LancaTarefa($Opportunity);
				
				$Email->setAssunto("CRM - Nova oportunidade");
				$Email->setModulo($Opportunity);
				$mensagem = "Olá ".$Email->getNome().". <br /><br /> Uma nova oportunidade foi registrada no SuiteCRM.<br /><br />";
				$mensagem .= "Segue o link abaixo para acessa-la.<br /><br />";
				$mensagem .= $this->url."/index.php?action=ajaxui#ajaxUILoc=index.php%3Fmodule%3DOpportunities%26offset%3D1%26stamp%3D1547140384068769300%26return_module%3DOpportunities%26action%3DDetailView%26record%3D".$Email->getModulo()->getId();
				$Email->setMensagem($mensagem);
				include("Email.php");
				$em = new Email();
				$em->CriaEmail($Email);
			}
		}
		/*
		*	RESPONSÁVEL POR ENVIAR OS DADOS DE ATIVIDADE PARA A CLASSE DE LANÇAMENTO DE ATIVIDADES.
		*
		*	$Opportunity -> Oportunidade criada.
		*/
		private function LancaTarefa($Opportunity)
		{
			include("models/Task_model.php");
			$Task = new Task_model();
			$Task->setAssunto("Enviar e-mail download de conteúdo");
			$Task->setDescricao("Enviar script Download de conteúdo para interagir com o lead e rastrear novas oportunidades.");
			$Task->setPrioridade("Medium");
			$Task->setModuloReferente("Opportunities");
			$Task->setModuloId($Opportunity->getId());
			$Task->setAtribuidoA($Opportunity->getAtribuidoA());
			include("generic/GenericTask.php");
			$TaskC = new GenericTask();
			$TaskC->CreateTask($Task);
		}
	}

	$r = new OportunidadeDownloadConteudoEntryPoint();
	$r->Load();