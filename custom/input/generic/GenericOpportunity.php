<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 23/01/2019
	*	VERSÃO: 1.0
	*	
	*	ESTA CLASSE TEM POR OBJETIVO REALIZAR A INSERÇÃO GENÉRICA DE OPORTUNIDADES
	*/

	class GenericOpportunity 
	{
		private $fileLog;//ponteiro para o arquivo de log
		protected $url;
		/*!
		*	RESPONSÁVEL POR CARREGAR AS DIRETRIZES INICIAIS.
		*/
		public function __construct()
		{
			date_default_timezone_set('America/Sao_Paulo');//seta o fuso horário correto
			$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/input/logs/exception.log","a");//define o arquivo de log
			$this->url = $GLOBALS['sugar_config']['site_url'];
		}
		/*!
		*	RESPONSÁVEL POR CRIAR UMA OPORTUNIDADE PARA UMA REVENDA OU PARA UM CLIENTE FINAL.
		*	
		*	$Opportunity -> Dados do lead enviando na conversão do formulário.
		*/
		public function CreateOpportunity($Opportunity)
		{
			try{
				$bean = BeanFactory::newBean('Opportunities');
				$bean->name = $Opportunity->getNome();
				$bean->assigned_user_id = $Opportunity->getAtribuidoA();
				$bean->description = $Opportunity->getDescricao();
				$bean->sales_stage = $Opportunity->getFaseVenda();
				$bean->lead_source = $Opportunity->getOrigem();
				$bean->account_id = $Opportunity->getIdClienteFinal();
				$bean->account_id_c = $Opportunity->getIdRevenda();

				$bean->save();
			}
			catch(Exception $e)
			{
				$texto = "\r\n----------EXCEPTION OPPORTUNITY--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n";
				$texto .= "Dados do RD: ";
				$texto .= "\r\n";
				$texto .= json_encode($dados->leads[0]);
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION OPPORTUNITY--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	RESPONSÁVEL POR VERIFICAR SE UMA OPORTUNIDADE JÁ EXISTE NO BANCO DE DADOS. SE EXITIR RETORNA A OPORTUNIDADE.
		*
		*	$Opportunity -> Oportunidade a ser verificada no banco.
		*/
		public function GetOpportunity($Opportunity, $email_lead = FALSE)
		{
			include(dirname(__FILE__)."\..\settings\conexao.php");
			require_once(dirname(__FILE__)."\..\models\Opportunity_model.php");

			$Op = new Opportunity_model();

			$deletado = 0;
			$descricao = $Opportunity->getDescricao();
			try
			{
				$query = $conexao->prepare("SELECT o.id, o.name, o.description, o.lead_source, o.assigned_user_id  
					FROM opportunities o 
					WHERE o.description = ? AND o.deleted = ?");
		
				$query->bindParam(1, $descricao);
				$query->bindParam(2, $deletado);
				
				$query->execute();
				
				if($query->rowCount() == 0)
					return null;
				else
				{
					$query = $query->fetch(PDO::FETCH_OBJ);
					$Opportunity->setId($query->id);
					return $Opportunity;
				}
			}
			catch(PDOException $e)
			{
				$texto = "\r\n----------EXCEPTION QUERY OPPORTUNITY--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n----------EXCEPTION QUERY OPPORTUNITY--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
				return new Opportunity_model();
			}
		}
		/*!
		*	RESPONSÁVEL POR ESCREVER O LOG NO ARQUIVO.
		*	
		*	$texto -> Contém o texto de log a ser escrito.
		*/
		function WriteFile($texto)
		{
			fwrite($this->fileLog, $texto." \r\n");
		}
		/*!
		*	RSPONSÁVEL POR DESTRUIR O PONTEIRO QUE CONTÉM A INSTÂNCIA DO ARQUIVO DE LOG.
		*/
		public function __destruct()
		{
			fclose($this->fileLog);
		}
	}