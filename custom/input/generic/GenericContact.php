<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 29/01/2019
	*	VERSÃO: 1.0
	*	
	*	ESTA CLASSE TEM POR OBJETIVO REALIZAR O GERENCIAMENTO DE CONTATOS DAS CONTAS DE CLIENTE/REVENDA.
	*/

	class GenericContact 
	{
		private $fileLog;//ponteiro para o arquivo de log
		protected $url;
		/*!
		*	RESPONSÁVEL POR CARREGAR AS DIRETRIZES INICIAIS.
		*/
		public function __construct()
		{
			date_default_timezone_set('America/Sao_Paulo');//seta o fuso horário correto
			$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/input/logs/exception.log","a");//define o arquivo de log
			$this->url = $GLOBALS['sugar_config']['site_url'];
		}
		/*!
		*	RESPONSÁVEL POR CRIAR UM NOVO CONTATO DA REVENDA OU DO CLIENTE FINAL.
		*	
		*	$Contact -> Objeto que contém os dados de contato a serem cadastrados.
		*/
		public function CreateContact($Contact)
		{
			try
			{
				$bean = BeanFactory::newBean('Contacts');
				
				$bean->first_name = $Contact->getNome();
				$bean->email1 = $Contact->getEmail();
				$bean->phone_work = $Contact->getTelefoneComercial();
				$bean->title = $Contact->getCargo();
				$bean->account_id = $Contact->getContaId();
				$bean->primary_address_city = $Contact->getCidade();
				$bean->primary_address_state = $Contact->getEstado();
				$bean->alt_address_city = $Contact->getCidade();
				$bean->alt_address_state = $Contact->getEstado();
				$bean->assigned_user_id = $Contact->getAtribuidoA();
				$bean->lead_source = $Contact->getOrigem();
				$bean->save();
			}
			catch(Exception $e)
			{
				$texto = "\r\n----------EXCEPTION CREATE CONTACT--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION CREATE CONTACT--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	RESPONSÁVEL POR ATUALIZAR OS DADOS DE UM CONTATO (CLIENTE OU REVENDA), TODA VEZ QUE ENTRAR UMA OPORTUNIDADE.
		*	
		*	$Contact -> Dados do contato a ser atualizado.
		*/
		public function UpdateContact($Contact)
		{
			try
			{
				$bean = BeanFactory::getBean('Contacts', $Contact->getId());
				
				$bean->first_name = $Contact->getNome();
				$bean->email1 = $Contact->getEmail();
				$bean->phone_work = $Contact->getTelefoneComercial();
				$bean->title = $Contact->getCargo();
				$bean->account_id = $Contact->getContaId();
				$bean->primary_address_city = $Contact->getCidade();
				$bean->primary_address_state = $Contact->getEstado();
				$bean->alt_address_city = $Contact->getCidade();
				$bean->alt_address_state = $Contact->getEstado();
				$bean->assigned_user_id = $Contact->getAtribuidoA();
				$bean->lead_source = $Contact->getOrigem();
				$bean->save();
			}
			catch(Exception $e)
			{
				$texto = "\r\n----------EXCEPTION UPDATE CONTACT--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION UPDATE CONTACT--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	RESPONSÁVEL POR VERIFICAR SE UM DETERMINADO CONTATO JÁ EXISTE NO CRM.
		*	
		*	$emailLead -> E-mail do lead a ser procurado em algum contato.
		*	
		*	return -> Se encontra o contato, retorna o objeto referente a ele, caso contrário retorna nulo.
		*/
		public function GetContact($emailLead)
		{
			include(dirname(__FILE__)."\..\settings\conexao.php");
			require_once(dirname(__FILE__)."\..\models\Contact_model.php");

			$Contact = new Contact_model();

			$deletado = 0;
			try{
				$query = $conexao->prepare("SELECT c.id, c.first_name, ea.email_address, c.phone_work, 
					c.primary_address_city, c.primary_address_state, c.assigned_user_id, c.title, 
					c.lead_source FROM email_addresses ea 
					INNER JOIN email_addr_bean_rel eabr ON ea.id = eabr.email_address_id AND 
						UPPER(ea.email_address) = UPPER(?) AND eabr.deleted = ? 
					INNER JOIN contacts c ON c.id = eabr.bean_id ");

				$query->bindParam(1, $emailLead);
				$query->bindParam(2, $deletado);
				
				$query->execute();
				
				if($query->rowCount() == 0)
					return null;
				else 
				{
					$query = $query->fetch(PDO::FETCH_OBJ);
					$Contact->setId($query->id);
					$Contact->setNome($query->first_name);
					$Contact->setEmail($query->email_address);
					$Contact->setCidade($query->primary_address_city);
					$Contact->setTelefoneComercial($query->phone_work);
					$Contact->setCargo($query->title);
					$Contact->setEstado($query->primary_address_state);
					$Contact->setOrigem($query->lead_source);
					$Contact->setAtribuidoA($query->assigned_user_id);
					
					return $Contact;
				}
			}
			catch(PDOException $e)
			{
				$texto = "\r\n----------EXCEPTION QUERY CONTACT--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n----------EXCEPTION QUERY CONTACT--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
				return null;
			}
		}
		/*!
		*	RESPONSÁVEL POR ESCREVER O LOG NO ARQUIVO.
		*	
		*	$texto -> Contém o texto de log a ser escrito.
		*/
		function WriteFile($texto)
		{
			fwrite($this->fileLog, $texto." \r\n");
		}
		/*!
		*	RSPONSÁVEL POR DESTRUIR O PONTEIRO QUE CONTÉM A INSTÂNCIA DO ARQUIVO DE LOG.
		*/
		public function __destruct()
		{
			fclose($this->fileLog);
		}
	}