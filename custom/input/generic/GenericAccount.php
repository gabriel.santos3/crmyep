<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 22/01/2019
	*	VERSÃO: 1.0
	*	
	*	ESTA CLASSE TEM POR OBJETIVO REALIZAR O CADASTRO OU ATUALIZAÇÃO DE UMA DETERMINADA CONTA (CLIENTE OU REVENDA)
	*/

	class GenericAccount
	{
		private $fileLog;//ponteiro para o arquivo de log

		/*!
		*	RESPONSÁVEL POR CARREGAR AS DIRETRIZES INICIAIS.
		*/
		public function __construct()
		{
			date_default_timezone_set('America/Sao_Paulo');//seta o fuso horário correto
			$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/input/logs/exception.log","a");//define o arquivo de log
		}
		/*!
		*	RESPONSÁVEL POR CRIAR UMA NOVA CONTA DA REVENDA OU DO CLIENTE FINAL.
		*	
		*	$Account -> Objeto que contém os dados de conta a serem cadastrados.
		*/
		public function CreateAccount($Account)
		{
			try
			{
				$bean = BeanFactory::newBean('Accounts');
				
				$bean->name = $Account->getEmpresa();
				$bean->email1 = $Account->getEmail();
				$bean->cnpj_c = $Account->getCnpj();
				$bean->phone_office = $Account->getTelefone();
				$bean->billing_address_city = $Account->getCidade();
				$bean->shipping_address_city = $Account->getCidade();
				$bean->billing_address_state = $Account->getEstado();
				$bean->shipping_address_state = $Account->getEstado();
				$bean->assigned_user_id = $Account->getAtribuidoA();
				$bean->account_type = $Account->getTipo();
				$bean->save();
			}
			catch(Exception $e)
			{
				$texto = "\r\n----------EXCEPTION CREATE ACCOUNT--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION CREATE ACCOUNT--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	RESPONSÁVEL POR ATUALIZAR OS DADOS DE UMA CONTA (CLIENTE OU REVENDA), TODA VEZ QUE ENTRAR UMA OPORTUNIDADE.
		*	
		*	$Account -> Dados da conta a ser atualizada.
		*/
		public function UpdateAccount($Account)
		{
			try
			{
				$bean = BeanFactory::getBean('Accounts', $Account->getId());
				
				$bean->name = $Account->getEmpresa();
				$bean->email1 = $Account->getEmail();
				$bean->cnpj_c = $Account->getCnpj();
				$bean->phone_office = $Account->getTelefone();
				$bean->billing_address_city = $Account->getCidade();
				$bean->shipping_address_city = $Account->getCidade();
				$bean->billing_address_state = $Account->getEstado();
				$bean->shipping_address_state = $Account->getEstado();
				$bean->assigned_user_id = $Account->getAtribuidoA();
				$bean->account_type = $Account->getTipo();
				$bean->save();
			}
			catch(Exception $e)
			{
				$texto = "\r\n----------EXCEPTION UPDATE ACCOUNT--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION UPDATE ACCOUNT--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	RESPONSÁVEL POR VERIFICAR SE UMA DETERMINADA CONTA JÁ EXISTE NO CRM.
		*	
		*	$emailLead -> E-mail do lead a ser procurado em alguma conta.
		*	
		*	return -> Se encontra a conta, retorna o objeto referente a ela, caso contrário retorna nulo.
		*/
		public function GetAccount($emailLead)
		{
			include(dirname(__FILE__)."\..\settings\conexao.php");
			require_once(dirname(__FILE__)."\..\models\Account_model.php");

			$Account = new Account_model();

			$deletado = 0;
			try{
				$query = $conexao->prepare("SELECT a.id, a.name, ea.email_address, a.billing_address_city, 
					a.phone_office, a.billing_address_state, a.account_type, a.assigned_user_id	FROM email_addresses ea 
					INNER JOIN email_addr_bean_rel eabr ON ea.id = eabr.email_address_id AND 
						UPPER(ea.email_address) = UPPER(?) AND eabr.deleted = ? 
					INNER JOIN accounts a ON a.id = eabr.bean_id ");

				$query->bindParam(1, $emailLead);
				$query->bindParam(2, $deletado);
				
				$query->execute();
				
				if($query->rowCount() == 0)
					return null;
				else 
				{
					$query = $query->fetch(PDO::FETCH_OBJ);
					$Account->setId($query->id);
					//$Account->setNome($query->name);
					$Account->setEmpresa($query->name);
					$Account->setEmail($query->email_address);
					//$Account->setCnpj($query->cnpj_c);
					$Account->setCidade($query->billing_address_city);
					$Account->setTelefone($query->phone_office);
					$Account->setEstado($query->billing_address_state);
					$Account->setTipo($query->account_type);
					$Account->setAtribuidoA($query->assigned_user_id);
					
					return $Account;
				}
			}
			catch(PDOException $e)
			{
				$texto = "\r\n----------EXCEPTION QUERY ACCOUNT--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n----------EXCEPTION QUERY ACCOUNT--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
				return null;
			}
		}
		/*!
		*	RESPONSÁVEL POR ESCREVER O LOG NO ARQUIVO.
		*	
		*	$texto -> Contém o texto de log a ser escrito.
		*/
		function WriteFile($texto)
		{
			fwrite($this->fileLog, $texto." \r\n");
		}
		/*!
		*	RSPONSÁVEL POR DESTRUIR O PONTEIRO QUE CONTÉM A INSTÂNCIA DO ARQUIVO DE LOG.
		*/
		public function __destruct()
		{
			fclose($this->fileLog);
		}
	}