<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 24/01/2019
	*	VERSÃO: 1.0
	*	
	*	ESTA CLASSE TEM POR OBJETIVO REALIZAR O GERENCIAMENTO DE USUÁRIOS ATRIBUÍDOS AS OPORTUNIDADES.
	*/

	class GenericUser 
	{
		private $fileLog;//ponteiro para o arquivo de log
		protected $url;
		/*!
		*	RESPONSÁVEL POR CARREGAR AS DIRETRIZES INICIAIS.
		*/
		public function __construct()
		{
			date_default_timezone_set('America/Sao_Paulo');//seta o fuso horário correto
			$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/input/logs/exception.log","a");//define o arquivo de log
			$this->url = $GLOBALS['sugar_config']['site_url'];
		}
		/*!
		*	RESPONSÁVEL POR BUSCAR OS DADOS DE UM VENDEDOR NO BANCO DE DADOS.
		*
		*	$firstName -> Primeiro Nome do vendedor.
		*	$lastName -> Último nome do vendedor.
		*/
		public function GetUsuario($firstName, $lastName)
		{
			include(dirname(__FILE__)."\..\settings\conexao.php");
			require_once(dirname(__FILE__)."\..\models\Usuario_model.php");

			$us = new Usuario_model();

			try
			{
				$query = $conexao->query("
					SELECT u.id, u.first_name, u.last_name, ea.email_address FROM users u 
					INNER JOIN email_addr_bean_rel er on u.id = er.bean_id 
					INNER JOIN email_addresses ea on ea.id = er.email_address_id 
					WHERE u.first_name LIKE '%$firstName%' AND u.last_name LIKE '%$lastName%'");
				
				$query->execute();
				
				if($query->rowCount() == 0)
					return null;
				else
				{
					$query = $query->fetch(PDO::FETCH_OBJ);
					$us->setId($query->id);
					$us->setPrimeiroNome($query->first_name);
					$us->setUltimoNome($query->last_name);
					$us->setEmail($query->email_address);
					return $us;
				}
			}
			catch(PDOException $e)
			{
				$texto = "\r\n----------EXCEPTION QUERY USUARIO--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n----------EXCEPTION QUERY USUARIO--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
				return new Usuario_model();
			}
		}
		/*!
		*	RESPONSÁVEL POR ESCREVER O LOG NO ARQUIVO.
		*	
		*	$texto -> Contém o texto de log a ser escrito.
		*/
		function WriteFile($texto)
		{
			fwrite($this->fileLog, $texto." \r\n");
		}
		/*!
		*	RSPONSÁVEL POR DESTRUIR O PONTEIRO QUE CONTÉM A INSTÂNCIA DO ARQUIVO DE LOG.
		*/
		public function __destruct()
		{
			fclose($this->fileLog);
		}
	}