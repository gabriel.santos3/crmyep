<?php 

	/*!
	*	AUTOR: TADEU TORRES
	*	DATA DA ÚLTIMA MODIFICAÇÃO 28/01/2019
	*	VERSÃO: 1.0
	*
	*	ESTA CLASSE É RESPONSÁVEL POR GERENCIAR TUDO SOBRE AS TAREFAS RELACIONADAS AS OPORTUNIDADES.
	*/

	class GenericTask
	{
		private $fileLog;

		public function __construct()
		{
			date_default_timezone_set('America/Sao_Paulo');//seta o fuso horário correto
			
			$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/input/logs/exception.log","a");//define o arquivo de log
		}
		/*!
		*	RESPONSÁVEL POR CRIAR UMA TAREFA E ASSOCIA-LA A UMA OPORTUNIDADE.
		*
		*	$Opportunity -> Objeto oportunidade.
		*	$Task -> Objeto tarefa que contém os dados da tarefa a ser cadastrada.
		*/
		public function CreateTask($Task)
		{
			try
			{
				$bean = BeanFactory::newBean('Tasks');
				
				$bean->name = $Task->getAssunto();
				$bean->status = $Task->getStatus();
				$bean->parent_type = $Task->getModuloReferente();
				$bean->parent_id = $Task->getModuloId();
				$bean->priority = $Task->getPrioridade();
				$bean->description = $Task->getDescricao();
				$bean->assigned_user_id = $Task->getAtribuidoA();
				$bean->save();
			}
			catch(Exception $e)
			{
				$texto = "\r\n----------EXCEPTION CREATE TASK--------".date("Y/m/d H:i:s")."---\r\n";
				$texto .= "Mensagem: ".$e->getMessage()."\r\n";
				$texto .= "\r\n";
				$texto .= "\r\n----------EXCEPTION CREATE TASK--------".date("Y/m/d H:i:s")."---\r\n";
				$this->WriteFile($texto);
			}
		}
		/*!
		*	ESCREVE O LOG NO ARQUIVO.
		*	
		*	$texto -> Contém o texto de log a ser escrito.
		*/
		function WriteFile($texto)
		{
			fwrite($this->fileLog, $texto." \r\n");
		}
		/*!
		*	RSPONSÁVEL POR DESTRUIR O PONTEIRO QUE CONTÉM A INSTÂNCIA DO ARQUIVO DE LOG.
		*/
		public function __destruct()
		{
			fclose($this->fileLog);
		}
	}