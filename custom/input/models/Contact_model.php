<?php 

class Contact_model 
{
	private $Id;
	private $Nome;
	private $Email;
	private $TelefoneComercial;
	private $Cargo;
	private $ContaId;
	private $Cidade;
	private $Estado;
	private $Origem;
	private $AtribuidoA;
	
	public function __construct()
	{
		$this->Id = null;
		$this->Nome = null;
		$this->Email = null;
		$this->TelefoneComercial = null;
		$this->Cargo = null;
		$this->ContaId = null;
		$this->Cidade = null;
		$this->Estado = null;
		$this->Origem = null;
		$this->AtribuidoA = null;
	}
	
	public function setId($Id)
	{
		$this->Id = $Id;
	}
	
	public function getId()
	{
		return $this->Id;
	}
	
	public function setNome($Nome)
	{
		$this->Nome = $Nome;
	}
	
	public function getNome()
	{
		return $this->Nome;
	}
	
	public function setEmail($Email)
	{
		$this->Email = $Email;
	}
	
	public function getEmail()
	{
		return $this->Email;
	}
	
	public function setTelefoneComercial($TelefoneComercial)
	{
		$this->TelefoneComercial = $TelefoneComercial;
	}
	
	public function getTelefoneComercial()
	{
		return $this->TelefoneComercial;
	}
	
	public function setCargo($Cargo)
	{
		$this->Cargo = $Cargo;
	}
	
	public function getCargo()
	{
		return $this->Cargo;
	}
	
	public function setContaId($ContaId)
	{
		$this->ContaId = $ContaId;
	}
	
	public function getContaId()
	{
		return $this->ContaId;
	}
	
	public function setCidade($Cidade)
	{
		$this->Cidade = $Cidade;
	}
	
	public function getCidade()
	{
		return $this->Cidade;
	}
	
	public function setEstado($Estado)
	{
		$this->Estado = $Estado;
	}
	
	public function getEstado()
	{
		return $this->Estado;
	}
	
	public function setOrigem($Origem)
	{
		$this->Origem = $Origem;
	}
	
	public function getOrigem()
	{
		return $this->Origem;
	}
		
	public function setAtribuidoA($AtribuidoA)
	{
		$this->AtribuidoA = $AtribuidoA;
	}
	
	public function getAtribuidoA()
	{
		return $this->AtribuidoA;
	}
}