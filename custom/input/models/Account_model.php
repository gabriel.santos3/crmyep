<?php 

class Account_model 
{
	private $Id;
	private $Nome;
	private $Empresa;
	private $Email;
	private $Cnpj;
	private $Cidade;
	private $Telefone;
	private $Estado;
	private $Tipo;
	private $AtribuidoA;
	
	public function __construct()
	{
		$this->Id = null;
		$this->Nome = null;
		$this->Email = null;
		$this->Empresa = null;
		$this->Cnpj = null;
		$this->Cidade = null;
		$this->Telefone = null;
		$this->Estado = null;
		$this->Tipo = null;
		$this->AtribuidoA = null;
	}
	
	public function setId($Id)
	{
		$this->Id = $Id;
	}
	
	public function getId()
	{
		return $this->Id;
	}
	
	public function setNome($Nome)
	{
		$this->Nome = $Nome;
	}
	
	public function getNome()
	{
		return $this->Nome;
	}
		
	public function setEmpresa($Empresa)
	{
		$this->Empresa = $Empresa;
	}
	
	public function getEmpresa()
	{
		return $this->Empresa;
	}
	
	public function setEmail($Email)
	{
		$this->Email = $Email;
	}
	
	public function getEmail()
	{
		return $this->Email;
	}
	
	public function setCnpj($Cnpj)
	{
		$this->Cnpj = $Cnpj;
	}
	
	public function getCnpj()
	{
		return $this->Cnpj;
	}
	
	public function setCidade($Cidade)
	{
		$this->Cidade = $Cidade;
	}
	
	public function getCidade()
	{
		return $this->Cidade;
	}
	
	public function setTelefone($Telefone)
	{
		$this->Telefone = $Telefone;
	}
	
	public function getTelefone()
	{
		return $this->Telefone;
	}
	
	public function setEstado($Estado)
	{
		$this->Estado = $Estado;
	}
	
	public function getEstado()
	{
		return $this->Estado;
	}
	
	public function setTipo($Tipo)
	{
		$this->Tipo = $Tipo;
	}
	
	public function getTipo()
	{
		return $this->Tipo;
	}
		
	public function setAtribuidoA($AtribuidoA)
	{
		$this->AtribuidoA = $AtribuidoA;
	}
	
	public function getAtribuidoA()
	{
		return $this->AtribuidoA;
	}
}