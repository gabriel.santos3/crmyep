<?php 

class Usuario_model 
{
	private $Id;
	private $PrimeiroNome;
	private $UltimoNome;
	private $Email;
	
	public function __construct()
	{
		$this->Id = null;
		$this->PrimeiroNome = null;
		$this->UltimoNome = null;
		$this->Email = null;
	}
	
	public function setId($Id)
	{
		$this->Id = $Id;
	}
	
	public function getId()
	{
		return $this->Id;
	}
	
	public function setPrimeiroNome($PrimeiroNome)
	{
		$this->PrimeiroNome = $PrimeiroNome;
	}
	
	public function getPrimeiroNome()
	{
		return $this->PrimeiroNome;
	}
		
	public function setUltimoNome($UltimoNome)
	{
		$this->UltimoNome = $UltimoNome;
	}
	
	public function getUltimoNome()
	{
		return $this->UltimoNome;
	}
	
	public function setEmail($Email)
	{
		$this->Email = $Email;
	}
	
	public function getEmail()
	{
		return $this->Email;
	}
}