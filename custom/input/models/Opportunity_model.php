<?php 

class Opportunity_model 
{
	private $Id;
	private $Nome;
	private $DataCriacao;
	private $IdRevenda;
	private $IdClienteFinal;
	private $Origem;
	private $Descricao;
	private $AtribuidoA;
	private $FaseVenda;
	
	public function __construct()
	{
		$this->Nome = null;
		$this->DataCriacao = null;
		$this->IdRevenda = null;
		$this->IdClienteFinal = null;
		$this->Origem = null;
		$this->Descricao = null;
		$this->AtribuidoA = null;
		$this->FaseVenda = null;
	}
	
	public function setId($Id)
	{
		$this->Id = $Id;
	}
	
	public function getId()
	{
		return $this->Id;
	}
	
	public function setNome($Nome)
	{
		$this->Nome = $Nome;
	}
	
	public function getNome()
	{
		return $this->Nome;
	}
	
	public function setDataCriacao($DataCriacao)
	{
		$this->DataCriacao = $DataCriacao;
	}
	
	public function getDataCriacao()
	{
		return $this->DataCriacao;
	}
	
	public function setIdRevenda($IdRevenda)
	{
		$this->IdRevenda = $IdRevenda;
	}
	
	public function getIdRevenda()
	{
		return $this->IdRevenda;
	}
	
	public function setIdClienteFinal($IdClienteFinal)
	{
		$this->IdClienteFinal = $IdClienteFinal;
	}
	
	public function getIdClienteFinal()
	{
		return $this->IdClienteFinal;
	}
	
	public function setOrigem($Origem)
	{
		$this->Origem = $Origem;
	}
	
	public function getOrigem()
	{
		return $this->Origem;
	}
	
	public function setDescricao($Descricao)
	{
		$this->Descricao = $Descricao;
	}
	
	public function getDescricao()
	{
		return $this->Descricao;
	}
	
	public function setAtribuidoA($AtribuidoA)
	{
		$this->AtribuidoA = $AtribuidoA;
	}
	
	public function getAtribuidoA()
	{
		return $this->AtribuidoA;
	}
	
	public function setFaseVenda($FaseVenda)
	{
		$this->FaseVenda = $FaseVenda;
	}
	
	public function getFaseVenda()
	{
		return $this->FaseVenda;
	}
}