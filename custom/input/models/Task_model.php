<?php 

class Task_model 
{
	private $id;
	private $Assunto;
	private $Status;
	private $ModuloReferente;
	private $ModuloId;
	private $Prioridade;
	private $Descricao;
	private $AtribuidoA;
	
	public function __construct()
	{
		$this->Id = null;
		$this->Assunto = null;
		$this->Status = "Not Started";
		$this->ModuloReferente = null;
		$this->ModuloId = null;
		$this->Prioridade = null;
		$this->Descricao = null;
		$this->AtribuidoA = null;
	}
	
	public function setId($Id)
	{
		$this->Id = $Id;
	}
	
	public function getId()
	{
		return $this->Id;
	}
	
	public function setAssunto($Assunto)
	{
		$this->Assunto = $Assunto;
	}
	
	public function getAssunto()
	{
		return $this->Assunto;
	}
		
	public function setStatus($Status)
	{
		$this->Status = $Status;
	}
	
	public function getStatus()
	{
		return $this->Status;
	}
	
	public function setModuloReferente($ModuloReferente)
	{
		$this->ModuloReferente = $ModuloReferente;
	}
	
	public function getModuloReferente()
	{
		return $this->ModuloReferente;
	}
	
	public function setModuloId($ModuloId)
	{
		$this->ModuloId = $ModuloId;
	}
	
	public function getModuloId()
	{
		return $this->ModuloId;
	}
	
	public function setPrioridade($Prioridade)
	{
		$this->Prioridade = $Prioridade;
	}
	
	public function getPrioridade()
	{
		return $this->Prioridade;
	}
	
	public function setDescricao($Descricao)
	{
		$this->Descricao = $Descricao;
	}
	
	public function getDescricao()
	{
		return $this->Descricao;
	}
	
	public function setAtribuidoA($AtribuidoA)
	{
		$this->AtribuidoA = $AtribuidoA;
	}
	
	public function getAtribuidoA()
	{
		return $this->AtribuidoA;
	}
}