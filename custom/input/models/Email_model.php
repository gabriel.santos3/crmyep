<?php 

class Email_model 
{
	private $Nome;
	private $Email;
	private $Assunto;
	private $Mensagem;
	private $Modulo;
	
	public function __construct()
	{
		$this->Nome = null;
		$this->Email = null;
		$this->Assunto = null;
		$this->Mensagem = null;
		$this->Modulo = null;
	}
	
	public function setNome($Nome)
	{
		$this->Nome = $Nome;
	}
	
	public function getNome()
	{
		return $this->Nome;
	}
	
	public function setEmail($Email)
	{
		$this->Email = $Email;
	}
	
	public function getEmail()
	{
		return $this->Email;
	}
	
	public function setAssunto($Assunto)
	{
		$this->Assunto = $Assunto;
	}
	
	public function getAssunto()
	{
		return $this->Assunto;
	}
		
	public function setMensagem($Mensagem)
	{
		$this->Mensagem = $Mensagem;
	}
	
	public function getMensagem()
	{
		return $this->Mensagem;
	}
	
	public function setModulo($Modulo)
	{
		$this->Modulo = $Modulo;
	}
	
	public function getModulo()
	{
		return $this->Modulo;
	}
}