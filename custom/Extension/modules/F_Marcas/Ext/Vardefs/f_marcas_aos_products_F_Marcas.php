<?php
// created: 2016-10-06 16:23:29
$dictionary["F_Marcas"]["fields"]["f_marcas_aos_products"] = array (
  'name' => 'f_marcas_aos_products',
  'type' => 'link',
  'relationship' => 'f_marcas_aos_products',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
);
