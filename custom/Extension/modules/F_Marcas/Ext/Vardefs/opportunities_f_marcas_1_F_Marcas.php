<?php
// created: 2019-03-28 19:08:06
$dictionary["F_Marcas"]["fields"]["opportunities_f_marcas_1"] = array (
  'name' => 'opportunities_f_marcas_1',
  'type' => 'link',
  'relationship' => 'opportunities_f_marcas_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_f_marcas_1opportunities_ida',
);
$dictionary["F_Marcas"]["fields"]["opportunities_f_marcas_1_name"] = array (
  'name' => 'opportunities_f_marcas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_f_marcas_1opportunities_ida',
  'link' => 'opportunities_f_marcas_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["F_Marcas"]["fields"]["opportunities_f_marcas_1opportunities_ida"] = array (
  'name' => 'opportunities_f_marcas_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_f_marcas_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_F_MARCAS_TITLE',
);
