<?php
// created: 2019-03-28 19:08:06
$dictionary["Opportunity"]["fields"]["opportunities_f_marcas_1"] = array (
  'name' => 'opportunities_f_marcas_1',
  'type' => 'link',
  'relationship' => 'opportunities_f_marcas_1',
  'source' => 'non-db',
  'module' => 'F_Marcas',
  'bean_name' => 'F_Marcas',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_F_MARCAS_TITLE',
);
