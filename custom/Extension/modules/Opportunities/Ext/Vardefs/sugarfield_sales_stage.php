<?php
 // created: 2019-03-28 18:38:43
$dictionary['Opportunity']['fields']['sales_stage']['default']='RegistroOportunidade';
$dictionary['Opportunity']['fields']['sales_stage']['len']=100;
$dictionary['Opportunity']['fields']['sales_stage']['inline_edit']=true;
$dictionary['Opportunity']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['Opportunity']['fields']['sales_stage']['merge_filter']='disabled';

 ?>