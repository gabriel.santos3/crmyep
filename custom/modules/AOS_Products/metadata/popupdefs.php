<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Products',
    'varName' => 'AOS_Products',
    'orderBy' => 'aos_products.name',
    'whereClauses' => array (
  'name' => 'aos_products.name',
  'f_marcas_aos_products_name' => 'aos_products.f_marcas_aos_products_name',
  'aos_product_category_name' => 'aos_products.aos_product_category_name',
),
    'searchInputs' => array (
  1 => 'name',
  8 => 'f_marcas_aos_products_name',
  9 => 'aos_product_category_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'f_marcas_aos_products_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
    'id' => 'F_MARCAS_AOS_PRODUCTSF_MARCAS_IDA',
    'width' => '10%',
    'name' => 'f_marcas_aos_products_name',
  ),
  'aos_product_category_name' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_AOS_PRODUCT_CATEGORYS_NAME',
    'id' => 'AOS_PRODUCT_CATEGORY_ID',
    'link' => true,
    'width' => '10%',
    'name' => 'aos_product_category_name',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '25%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'AOS_PRODUCT_CATEGORY_NAME' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_AOS_PRODUCT_CATEGORYS_NAME',
    'id' => 'AOS_PRODUCT_CATEGORY_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'name' => 'aos_product_category_name',
  ),
  'F_MARCAS_AOS_PRODUCTS_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
    'id' => 'F_MARCAS_AOS_PRODUCTSF_MARCAS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'f_marcas_aos_products_name',
  ),
),
);
