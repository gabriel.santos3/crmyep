<?php
$module_name = 'AOS_Products';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'f_marcas_aos_products_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
        'width' => '10%',
        'default' => true,
        'id' => 'F_MARCAS_AOS_PRODUCTSF_MARCAS_IDA',
        'name' => 'f_marcas_aos_products_name',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'f_marcas_aos_products_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
        'id' => 'F_MARCAS_AOS_PRODUCTSF_MARCAS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'f_marcas_aos_products_name',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
