<?php
// created: 2018-09-10 17:37:35
$searchdefs['AOS_Products'] = array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
        'id' => 'F_MARCAS_AOS_PRODUCTSF_MARCAS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'f_marcas_aos_products_name',
      ),
    ),
    'advanced_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_F_MARCAS_TITLE',
        'id' => 'F_MARCAS_AOS_PRODUCTSF_MARCAS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'f_marcas_aos_products_name',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
    'maxColumnsBasic' => '3',
  ),
);