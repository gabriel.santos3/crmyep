<?php
// created: 2016-09-28 11:35:33
$mod_strings = array (
  'LNK_NEW_LEAD' => 'Novo Potencial',
  'LNK_LEAD_LIST' => 'Potenciais',
  'LNK_IMPORT_VCARD' => 'Criar a partir de um vCard',
  'LNK_IMPORT_LEADS' => 'Importar Potenciais',
  'LBL_LIST_FORM_TITLE' => 'Lista de Potenciais',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Lead',
  'LBL_REPORTS_TO' => 'Reporta a:',
);