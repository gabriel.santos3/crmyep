<?php
// created: 2017-05-15 15:48:29
$mod_strings = array (
  'LBL_LEADS' => 'Leads',
  'LBL_EDITVIEW_PANEL1' => 'Deep Profile',
  'LBL_COMIDAPREF' => 'Comida Preferida',
  'LBL_FONTEINF' => 'Fonte de Informação',
  'LBL_COMPLEMENTOHOBBY' => 'Outros hobbies',
  'LBL_HOBBY' => 'Hobby',
  'LBL_ESTADOCIVIL' => 'Estado Cívil',
  'LBL_GIFT' => 'Brinde',
  'LBL_MARCATEC' => 'Marca Preferida',
  'LBL_PREFLOCAL' => 'Local de Preferencia',
  'LBL_TIMEPREFERIDO' => 'Time do Coração',
  'LBL_COMPLEMENTOFONT' => 'Outros',
  'LBL_COMPLEMENTOCOMIDA' => 'Outras Comidas',
  'LBL_COMPLEMENTOMARCA' => 'Outras Marcas',
);