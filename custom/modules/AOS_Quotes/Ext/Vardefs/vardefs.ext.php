<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-10-28 15:59:02
$dictionary['AOS_Quotes']['fields']['stage']['required']=false;
$dictionary['AOS_Quotes']['fields']['stage']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['stage']['merge_filter']='disabled';

 

 // created: 2019-06-12 19:38:10
$dictionary['AOS_Quotes']['fields']['subtotal_amount']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['subtotal_amount']['merge_filter']='disabled';
$dictionary['AOS_Quotes']['fields']['subtotal_amount']['enable_range_search']=false;

 

 // created: 2019-06-12 19:37:47
$dictionary['AOS_Quotes']['fields']['parcelas_c']['inline_edit']='1';
$dictionary['AOS_Quotes']['fields']['parcelas_c']['labelValue']='Plots';

 

 // created: 2016-10-07 14:10:12
$dictionary['AOS_Quotes']['fields']['name']['required']=false;
$dictionary['AOS_Quotes']['fields']['name']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Quotes']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Quotes']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Quotes']['fields']['name']['unified_search']=false;

 

 // created: 2016-10-28 15:40:24
$dictionary['AOS_Quotes']['fields']['opportunity']['required']=true;
$dictionary['AOS_Quotes']['fields']['opportunity']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['opportunity']['merge_filter']='disabled';

 

 // created: 2016-10-28 15:59:12
$dictionary['AOS_Quotes']['fields']['expiration']['required']=false;
$dictionary['AOS_Quotes']['fields']['expiration']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['expiration']['merge_filter']='disabled';
$dictionary['AOS_Quotes']['fields']['expiration']['display_default']='';

 

 // created: 2019-06-12 19:38:27
$dictionary['AOS_Quotes']['fields']['total_amount']['inline_edit']=true;
$dictionary['AOS_Quotes']['fields']['total_amount']['merge_filter']='disabled';

 
?>