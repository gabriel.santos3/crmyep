<?php
// created: 2019-06-07 16:40:14
$mod_strings = array (
  'LBL_ACCOUNT_NAME' => 'Nome Cotação',
  'LBL_LINE_ITEMS' => 'Itens',
  'LNK_NEW_RECORD' => 'Criar Cotação',
  'LNK_LIST' => 'Exibir Detalhes da Oportunidade',
  'MSG_SHOW_DUPLICATES' => 'Criar esta conta pode potencialmente criar uma conta duplicada. Você pode clicar em Salvar para continuar a criar essa nova conta com os dados previamente inseridos ou clicar em Cancelar.',
  'LBL_LIST_FORM_TITLE' => 'Lista de Detalhes da Oportunidade',
  'LBL_SEARCH_FORM_TITLE' => 'Pesquisar Detalhes da Oportunidade',
  'LBL_BILLING_ACCOUNT' => 'Cliente',
  'LBL_ADDRESS_INFORMATION' => 'Cliente',
  'LBL_QUOTE_NUMBER' => 'Número do Detalhe',
  'LBL_STAGE' => 'Fase da Oportunidade',
  'LBL_NAME' => 'Título',
  'LBL_SUBTOTAL_AMOUNT' => 'Total Contrato',
  'LBL_EXPIRATION' => 'Data Faturamento',
  'LBL_OPPORTUNITY_OPPORTUNITY_ID' => 'Oportunidades (ID Oportunidade relacionado)',
  'LBL_OPPORTUNITY' => 'Oportunidades',
  'LBL_PARCELAS' => 'Parcelas',
  'LBL_GRAND_TOTAL' => 'Total Parcela',
);