function update(){
	document.getElementById('parcelas_c').addEventListener("change", myfunction());	
	document.getElementById('subtotal_amount').addEventListener("change", myfunction());	
}
function myfunction(){
	var subtotal = document.getElementById('subtotal_amount').value; //valor total do contrato
	var parcelas = document.getElementById('parcelas_c').value; //valor das parcelas
	if(subtotal.indexOf(",") < 0 && subtotal.indexOf(".")<0){
		subtotal = subtotal+",00";
	}else if(subtotal.indexOf(".")>0){
		subtotal = subtotal.replace('.',',');
	}
	document.getElementById('subtotal_amount').value = subtotal;
		
	subtotal = parseFloat(subtotal.replace(',','.'));//para efeito de calculos em js, substituir virgula por ponto
	
	var total = subtotal/parcelas;
	total = parseFloat(total.toFixed(2)); //fixar casas decimais em 2
	total = total.toString();
	
	total = total.replace('.',','); //modelo padrao de moeda brasileira com virgula separando reais de centavos
	if(total.indexOf(",") < 0){
		total = total+",00";
	}
	
	document.getElementById('total_amount').value = total;
}