<?php
// created: 2018-09-10 17:37:36
$searchdefs['AOS_Quotes'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
    'maxColumnsBasic' => '3',
  ),
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'type' => 'int',
        'label' => 'LBL_QUOTE_NUMBER',
        'width' => '10%',
        'default' => true,
        'name' => 'number',
      ),
      1 => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_BILLING_ACCOUNT',
        'id' => 'BILLING_ACCOUNT_ID',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'billing_account',
      ),
    ),
    'advanced_search' => 
    array (
      0 => 
      array (
        'name' => 'number',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'name' => 'billing_account',
        'default' => true,
        'width' => '10%',
      ),
      2 => 
      array (
        'name' => 'expiration',
        'default' => true,
        'width' => '10%',
      ),
      3 => 
      array (
        'name' => 'stage',
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
);