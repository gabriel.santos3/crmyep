<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2019-03-28 19:08:06
$dictionary["F_Marcas"]["fields"]["opportunities_f_marcas_1"] = array (
  'name' => 'opportunities_f_marcas_1',
  'type' => 'link',
  'relationship' => 'opportunities_f_marcas_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_OPPORTUNITIES_TITLE',
  'id_name' => 'opportunities_f_marcas_1opportunities_ida',
);
$dictionary["F_Marcas"]["fields"]["opportunities_f_marcas_1_name"] = array (
  'name' => 'opportunities_f_marcas_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_OPPORTUNITIES_TITLE',
  'save' => true,
  'id_name' => 'opportunities_f_marcas_1opportunities_ida',
  'link' => 'opportunities_f_marcas_1',
  'table' => 'opportunities',
  'module' => 'Opportunities',
  'rname' => 'name',
);
$dictionary["F_Marcas"]["fields"]["opportunities_f_marcas_1opportunities_ida"] = array (
  'name' => 'opportunities_f_marcas_1opportunities_ida',
  'type' => 'link',
  'relationship' => 'opportunities_f_marcas_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_F_MARCAS_TITLE',
);


// created: 2016-10-06 16:23:29
$dictionary["F_Marcas"]["fields"]["f_marcas_aos_products"] = array (
  'name' => 'f_marcas_aos_products',
  'type' => 'link',
  'relationship' => 'f_marcas_aos_products',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_F_MARCAS_AOS_PRODUCTS_FROM_AOS_PRODUCTS_TITLE',
);

?>