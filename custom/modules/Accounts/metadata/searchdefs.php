<?php
$searchdefs ['Accounts'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'cardcode_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CARDCODE',
        'width' => '10%',
        'name' => 'cardcode_c',
      ),
      'cnpj_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CNPJ',
        'width' => '10%',
        'name' => 'cnpj_c',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'cardcode_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CARDCODE',
        'width' => '10%',
        'name' => 'cardcode_c',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'cnpj_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_CNPJ',
        'width' => '10%',
        'name' => 'cnpj_c',
      ),
      'account_type' => 
      array (
        'name' => 'account_type',
        'default' => true,
        'width' => '10%',
      ),
      'industry' => 
      array (
        'name' => 'industry',
        'default' => true,
        'width' => '10%',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
