<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	ÚLTIMA ATUALIZAÇÃO: 22/01/2019
	*
	*	OBJETIVO: ESTA CLASSE TEM POR OBJETIVO ENVIAR UMA VENDA PARA O RD STATION DE ACORDO COM O LEAD QUE SE ENCONTRA  
	*	NA OPORTUNIDADE.
	*	É ENVIADO PARA O RD SOMENTE VENDA DE OPORTUNIDADES ORIGINADAS DO RD.
	*	BASICAMENTE, QUANDO O USUÁRIO ESTÁ EDITANDO UMA OPORTUNIDADE E CLICA NO BOTÃO DE SALVAR, O MÉTODO "ANALISADADOS" 
	*	DESTA CLASSE É DISPARADO ATRAVÉS DO LOGIC HOOK, ENTÃO É REALIZADO A ANÁLISE OS DADOS DA OPORTUNIDADE E SE PASSAR
	*	NA ANÁLISE OS DADOS SÃO ENVIADOS PARA O RD.
	*/

	//importa a classe de acesso a api do rd
	require_once("RDStationAPI.class.php");

class LancamentoVendaRd 
{
	private $email_lead;
	private $valor_venda;
	private $reason_lost;
	private $status_op;//ganha ou perdida
	private $publicToken;
	private $privateToken;
	private $rdAPI;
	private $url = "http://crm.primeinterway.com.br:808/";
	
	private $fileLog;
	
	/*!
	*	CRIA A INSTÂNCIA DE ACESSO A PLATAFORMA DO RD STATION.
	*/
	function __construct()
	{
		$this->url = $GLOBALS['sugar_config']['site_url'];
		
		date_default_timezone_set('America/Sao_Paulo');
		$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/modules/Opportunities/exception.log","a");
		$this->publicToken = "bf3b4155b51e11db4674124b24ae4f76";
		$this->privateToken = "4bad60e27cd8e01767d040fc01d682ff";
		try{
			$this->rdAPI = new RDStationAPI($this->privateToken, $this->publicToken);
		}
		catch(Exception $e)
		{
			$texto = "\r\n----------EXCEPTION--------".date("Y/m/d H:i:s")."---\r\n";
			$texto .= "Mensagem: ".$e->getMessage();
			$texto .= "\r\n----------EXCEPTION--------".date("Y/m/d H:i:s")."---\r\n";
			$this->WriteFile($texto);
			//header("LOCATION: ".$this->url);
		}
	}
	/*!
	*	RECEBE OS DADOS DA OPORTUNIDADE SALVA PELO VENDEDOR.
	*
	*	$bean -> Contém os dados da oportunidade salva pelo vendedor.
	*	$event -> Contém o nome do evento.
	*	$arguments -> Contém detalhes de módulos relacionados.
	*/
	function AnalisaDados($bean, $event, $arguments)
	{
		if(!empty($bean->account_id_c))//se existir uma revenda associada a oportunidade.
		{
			$account = BeanFactory::getBean('Accounts', $bean->account_id_c);//id da revenda
			$this->email_lead = $account->email1;
			$this->valor_venda = $bean->amount;
			
			if($bean->sales_stage == "PedidoRec")//verifica se o estágio da oportunidade está marcado como venda
			{
				$this->status_op = "won";//won or lost
				$this->reason_lost = "";
				$this->EnviaVendaRd($bean);
			}
			else if($bean->sales_stage == "Perdido")
			{
				$this->status_op = "lost";//won or lost
				$this->reason_lost = "Oportunidade perdida. Valor: R$ ".number_format($bean->amount, 2, ',', '.');
				$this->EnviaVendaRd($bean);
			}
		}
	}
	/*!
	*	ENVIA A VENDA PARA O RD. INFORMA O LEAD, BEM COMO O STATUS (WON) E O VALOR DA VENDA.
	*	
	*	$op -> Oportunidade que está sendo salva pelo usuário
	*/
	function EnviaVendaRd($op)
	{
		try{
			$return = $this->rdAPI->updateLeadStatus($this->email_lead, $this->status_op, $this->valor_venda, $this->reason_lost);
		}
		catch(Exception $e)
		{
			$texto = "\r\n----------EXCEPTION--------".date("Y/m/d H:i:s")."---\r\n";
			$texto .= "Mensagem: ".$e->getMessage()."\r\n";
			$texto .= "Id da oportunidade: ".$op->id."\r\n";
			$texto .= "Nome da oportunidade: ".$op->name;
			$texto .= "\r\n----------EXCEPTION--------".date("Y/m/d H:i:s")."---\r\n";
			$this->WriteFile($texto);
		}
	}
	/*!
	*	ESCREVE O LOG NO ARQUIVO.
	*	
	*	$texto -> Contém o texto de log.
	*/
	function WriteFile($texto)
	{
		fwrite($this->fileLog, $texto." \r\n");
	}
	/*!
	*	FECHA AS INSTÂNCIAS ABERTAS
	*/ 
	function __destruct()
	{
		fclose($this->fileLog);
	}
}