<?php
	/*!
	*	AUTOR: TADEU TORRES
	*	ÚLTIMA ATUALIZAÇÃO: 28/01/2019
	*
	*	OBJETIVO: ESTA CLASSE TEM POR OBJETIVO ATRIBUIR AO VENDEDOR TODAS AS TAREFAS REFEFENTES 
	*	A UMA OPORTUNIDADE.
	*/
class Tarefa 
{
	private $url = "http://crm.primeinterway.com.br:808/";
	
	private $fileLog;
	
	/*!
	*	CRIA A INSTÂNCIA DE ACESSO A PLATAFORMA DO RD STATION.
	*/
	function __construct()
	{
		$this->url = $GLOBALS['sugar_config']['site_url'];
		
		date_default_timezone_set('America/Sao_Paulo');
		$this->fileLog = fopen("C:/xampp/htdocs/crmAlpha/custom/modules/Opportunities/exception.log","a");
	}
	/*!
	*	RESPONSÁVEL POR ATRIBUIR TODAS AS TAREFAS DE UMA  OPORTUNIDADE AO VENDEDOR ATRIBUIDO NA OPORTUNIDADE.
	*
	*	$bean -> Contém os dados da oportunidade salva pelo vendedor.
	*	$event -> Contém o nome do evento.
	*	$arguments -> Contém detalhes de módulos relacionados.
	*/
	function Atribuir($bean, $event, $arguments)
	{
		require("C:/xampp/htdocs/crmAlpha/custom/input/settings/defines.php");
		
		$accountBean = BeanFactory::getBean('Opportunities', $bean->id);
		$tasks = $accountBean->get_linked_beans(
			'tasks',
			'Tasks',
			array(),
			0,
			10,
			0,
			"");
		$enviaEmail = false;
		for($i = 0; $i < COUNT($tasks); $i++)	
		{
			$taskBean = BeanFactory::getBean('Tasks', $tasks[$i]->id);
			//garante que o e-mail seja enviado apenas quando houver modificação do usuário atribuído a oportunidade
			//evitando assim enviar e-mails toda vez que se salva alguma alteração na oportunidade.
			if($taskBean->assigned_user_id != $bean->assigned_user_id)
				$enviaEmail = true;
			$taskBean->assigned_user_id = $bean->assigned_user_id;
			$taskBean->save();
			
		}
		if($enviaEmail === true)
			$this->NotificaVendedor($bean);
	}
	/*!
	*	RESPONSÁVEL POR ENVIAR UM E-MAIL PARA O VENDEDOR, AVISANDO-O QUE UMA 
	*	OPORTUNIDADE FOI ATRIBUÍDA A ELE.
	*
	*	$Opportunity -> Oportunidade atribuída ao vendedor.
	*/
	function NotificaVendedor($Opportunity)
	{
		require("C:/xampp/htdocs/crmAlpha/custom/input/models/Email_model.php");
		$Email = new Email_model();
		$Email->setAssunto("CRM - Nova oportunidade");
		$Email->setModulo($Opportunity);
		$userBean = BeanFactory::getBean("Users", $Opportunity->assigned_user_id);
		$Email->setNome($userBean->first_name);
		$Email->setEmail($userBean->email1);
		
		$mensagem = "Olá ".$Email->getNome().". <br /><br /> Uma nova oportunidade foi atribuída a você.<br /><br />";
		$mensagem .= "Segue o link abaixo para acessa-la.<br /><br />";
		$mensagem .= $this->url."/index.php?action=ajaxui#ajaxUILoc=index.php%3Fmodule%3DOpportunities%26offset%3D1%26stamp%3D1547140384068769300%26return_module%3DOpportunities%26action%3DDetailView%26record%3D".$Email->getModulo()->id;
		$Email->setMensagem($mensagem);
		
		require("C:/xampp/htdocs/crmAlpha/custom/input/Email.php");
		$em = new Email();
		$em->CriaEmail($Email);
	}
	/*!
	*	ESCREVE O LOG NO ARQUIVO.
	*	
	*	$texto -> Contém o texto de log.
	*/
	function WriteFile($texto)
	{
		fwrite($this->fileLog, $texto." \r\n");
	}
	/*!
	*	FECHA AS INSTÂNCIAS ABERTAS
	*/ 
	function __destruct()
	{
		fclose($this->fileLog);
	}
}