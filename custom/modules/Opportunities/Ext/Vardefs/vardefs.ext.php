<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-05-21 20:35:33
$dictionary['Opportunity']['fields']['revenda_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['revenda_c']['labelValue']='Revenda';

 

 // created: 2017-05-11 17:59:21
$dictionary['Opportunity']['fields']['user_id1_c']['inline_edit']=1;

 

 // created: 2019-06-24 13:36:14
$dictionary['Opportunity']['fields']['loctyperadio_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['loctyperadio_c']['labelValue']='Tipo de locação';

 

 // created: 2016-09-27 09:21:25
$dictionary['Opportunity']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2019-03-28 18:38:43
$dictionary['Opportunity']['fields']['sales_stage']['default']='RegistroOportunidade';
$dictionary['Opportunity']['fields']['sales_stage']['len']=100;
$dictionary['Opportunity']['fields']['sales_stage']['inline_edit']=true;
$dictionary['Opportunity']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['Opportunity']['fields']['sales_stage']['merge_filter']='disabled';

 

 // created: 2016-09-28 11:43:01
$dictionary['Opportunity']['fields']['account_id_c']['inline_edit']=1;

 

// created: 2019-03-28 19:08:06
$dictionary["Opportunity"]["fields"]["opportunities_f_marcas_1"] = array (
  'name' => 'opportunities_f_marcas_1',
  'type' => 'link',
  'relationship' => 'opportunities_f_marcas_1',
  'source' => 'non-db',
  'module' => 'F_Marcas',
  'bean_name' => 'F_Marcas',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_F_MARCAS_TITLE',
);


 // created: 2017-05-11 17:56:41
$dictionary['Opportunity']['fields']['user_id_c']['inline_edit']=1;

 

 // created: 2019-05-21 18:00:34
$dictionary['Opportunity']['fields']['typesale_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['typesale_c']['labelValue']='Tipo de Oportunidade';

 

 // created: 2017-05-11 17:59:21
$dictionary['Opportunity']['fields']['intermediario2_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['intermediario2_c']['labelValue']='Intermediário 2';

 

 // created: 2016-09-27 09:21:26
$dictionary['Opportunity']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2016-09-28 16:33:35
$dictionary['Opportunity']['fields']['probability']['inline_edit']='';
$dictionary['Opportunity']['fields']['probability']['comments']='The probability of closure';
$dictionary['Opportunity']['fields']['probability']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['probability']['enable_range_search']=false;
$dictionary['Opportunity']['fields']['probability']['min']=0;
$dictionary['Opportunity']['fields']['probability']['max']=100;
$dictionary['Opportunity']['fields']['probability']['disable_num_format']='';

 

 // created: 2016-09-27 09:21:25
$dictionary['Opportunity']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2016-09-27 09:21:26
$dictionary['Opportunity']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2017-05-11 17:56:41
$dictionary['Opportunity']['fields']['intermediario1_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['intermediario1_c']['labelValue']='Intermediario 1';

 

 // created: 2019-01-30 14:20:06
$dictionary['Opportunity']['fields']['status_demonstracao_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['status_demonstracao_c']['labelValue']='Status da demonstração';

 

 // created: 2016-12-13 17:10:04
$dictionary['Opportunity']['fields']['lead_source']['len']=100;
$dictionary['Opportunity']['fields']['lead_source']['required']=true;
$dictionary['Opportunity']['fields']['lead_source']['inline_edit']=true;
$dictionary['Opportunity']['fields']['lead_source']['comments']='Source of the opportunity';
$dictionary['Opportunity']['fields']['lead_source']['merge_filter']='disabled';

 

 // created: 2019-03-28 19:25:09
$dictionary['Opportunity']['fields']['mark_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['mark_c']['labelValue']='Marca';

 

 // created: 2019-03-28 19:25:08
$dictionary['Opportunity']['fields']['f_marcas_id_c']['inline_edit']=1;

 

 // created: 2019-05-22 16:15:56
$dictionary['Opportunity']['fields']['contractduration_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['contractduration_c']['labelValue']='Duração do Contrato';

 

 // created: 2019-04-10 18:04:07
$dictionary['Opportunity']['fields']['sales_prediction_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['sales_prediction_c']['options']='date_range_search_dom';
$dictionary['Opportunity']['fields']['sales_prediction_c']['labelValue']='Previsão de faturamento ';
$dictionary['Opportunity']['fields']['sales_prediction_c']['enable_range_search']='1';

 
?>