<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-03-28 19:08:06
$layout_defs["Opportunities"]["subpanel_setup"]['opportunities_f_marcas_1'] = array (
  'order' => 100,
  'module' => 'F_Marcas',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_OPPORTUNITIES_F_MARCAS_1_FROM_F_MARCAS_TITLE',
  'get_subpanel_data' => 'opportunities_f_marcas_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Opportunities']['subpanel_setup']['opportunity_aos_quotes']['override_subpanel_name'] = 'Opportunity_subpanel_opportunity_aos_quotes';

?>