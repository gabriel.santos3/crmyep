<?php
$viewdefs ['Opportunities'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'includes' => 
      array (
        0 => 
        array (
          'file' => '/custom/modules/Opportunities/JS/updateOP.js',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '{$PROBABILITY_SCRIPT}',
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
          ),
          1 => 'account_name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'mark_c',
            'studio' => 'visible',
            'label' => 'LBL_MARK',
          ),
          1 => 
          array (
            'name' => 'sales_prediction_c',
            'label' => 'LBL_SALES_PREDICTION ',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'currency_id',
            'label' => 'LBL_CURRENCY',
          ),
          1 => 
          array (
            'name' => 'date_closed',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'revenda_c',
            'studio' => 'visible',
            'label' => 'LBL_REVENDA',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'typesale_c',
            'studio' => 'visible',
            'label' => 'LBL_TYPESALE',
            'displayParams' => 
            array (
              'field' => 
              array (
                'onChange' => 'updateOP();',
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'loctyperadio_c',
            'studio' => 'visible',
            'label' => 'LBL_LOCTYPERADIO',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'amount',
            'label' => 'LBL_AMOUNT',
            'displayParams' => 
            array (
              'field' => 
              array (
                'onChange' => 'updateOP();',
              ),
            ),
          ),
          1 => 
          array (
            'name' => 'contractduration_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTRACTDURATION',
          ),
        ),
        6 => 
        array (
          0 => 'sales_stage',
          1 => 'lead_source',
        ),
        7 => 
        array (
          0 => 'probability',
          1 => 'campaign_name',
        ),
        8 => 
        array (
          0 => 'next_step',
        ),
        9 => 
        array (
          0 => 'description',
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
;
?>
