<?php
// created: 2016-10-21 19:37:06
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Opportunities',
      4 => 'AOS_Quotes',
    ),
  ),
  'LBL_TABGROUP_MARKETING' => 
  array (
    'label' => 'LBL_TABGROUP_MARKETING',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Campaigns',
      4 => 'Prospects',
      5 => 'ProspectLists',
    ),
  ),
  'LBL_TABGROUP_ACTIVITIES' => 
  array (
    'label' => 'LBL_TABGROUP_ACTIVITIES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Calendar',
      2 => 'Calls',
      3 => 'Meetings',
      4 => 'Emails',
      5 => 'Tasks',
      6 => 'Notes',
    ),
  ),
  'LBL_TABGROUP_COLLABORATION' => 
  array (
    'label' => 'LBL_TABGROUP_COLLABORATION',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Emails',
      2 => 'Documents',
      3 => 'Project',
    ),
  ),
  'LBL_GROUPTAB4_1477085826' => 
  array (
    'label' => 'LBL_GROUPTAB4_1477085826',
    'modules' => 
    array (
      0 => 'AOR_Reports',
    ),
  ),
);